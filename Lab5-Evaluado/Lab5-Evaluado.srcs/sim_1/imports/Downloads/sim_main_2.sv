`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06.05.2019 17:58:31
// Design Name: 
// Module Name: sim_main_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sim_main_2();
    logic  clk;
    logic  [3:1] JA ;
    
   main_2 uut(.CLK100MHZ(clk), .JA(JA));
    
    always #5 clk = ~clk; // Generacion senal de reloj
    
    initial begin
    clk = 0;
    end
    
endmodule
