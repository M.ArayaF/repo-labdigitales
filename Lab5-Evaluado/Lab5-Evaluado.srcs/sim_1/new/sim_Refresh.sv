`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/06/2019 06:33:23 PM
// Design Name: 
// Module Name: sim_Refresh
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sim_Refresh();
    logic clk;
    logic [2:0] out;
    logic [7:0] an;
    Refresh dut(.clk(clk),.out(out));
    Demux cuatro(.control(out), .out(an));

    always #5 clk =~clk;
    initial begin
    clk=0;
    end
endmodule
