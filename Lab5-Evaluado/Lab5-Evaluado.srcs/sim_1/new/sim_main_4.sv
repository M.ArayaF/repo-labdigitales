`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/05/2019 12:20:52 AM
// Design Name: 
// Module Name: sim_main_4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sim_main_4();
    logic [7:0] salida;
    ALU picopalquelee (.OpCode(4'b0100),.operator_1(8'd4),.operator_2(8'd4),.result(salida));
endmodule
