`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/04/2019 07:15:45 PM
// Design Name: 
// Module Name: sim_BCD
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sim_main_1();
    logic CA,CB,CC,CD,CE,CF,CG;
    logic [7:0] AN;
    logic clk;
    
    main_1 UTT(.SW(4'd10), 
    .CA(CA), .CB(CB), .CC(CC), .CD(CD), .CE(CE), .CF(CF), .CG(CG),
    .AN(AN), .CLK100MHZ(clk) );
    
        always #5 clk =~clk;
    initial begin
    clk=0;
    end
endmodule