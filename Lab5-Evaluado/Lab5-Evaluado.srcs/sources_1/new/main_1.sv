`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/04/2019 08:10:06 PM
// Design Name: 
// Module Name: main_1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module main_1(
    input logic CLK100MHZ,
    input logic [3:0] SW,
    output logic CA,CB,CC,CD,CE,CF,CG,
    output logic [7:0] AN,
    output logic DP
    );
    logic [2:0] cnt_demux;
    logic freq_inter; 
    assign DP = 1'b1;
    
    BCD_to_7Seg uno(.BCD_in({SW[3], SW[2], SW[1], SW[0]}), 
    .sevenSeg({CG,CF,CE,CD,CC,CB,CA}));
    
    Divisor #(50000000) dos(.clk_in(CLK100MHZ),.clk_out(freq_inter)); // 1 hz
    Refresh tres(.clk(freq_inter), .out(cnt_demux));
    Demux cuatro(.control(cnt_demux), .out(AN));
    
endmodule