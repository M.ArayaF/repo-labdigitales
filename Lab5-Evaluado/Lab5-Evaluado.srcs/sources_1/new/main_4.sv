`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/05/2019 12:19:48 AM
// Design Name: 
// Module Name: main_4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module main_4(
    input logic [15:0] SW,
    input logic BTNU, BTNL, BTNR, BTND,
    input   logic CLK100MHZ,
    output logic [15:0] LED,
    output logic CA,CB,CC,CD,CE,CF,CG,
    output logic [7:0] AN,
    output logic DP
    );
    logic [7:0] result;
    logic [2:0] cable_state;
    
    ALU UTT(.OpCode({BTNU, BTNL, BTNR, BTND}),
     .operator_1({SW[7],SW[6],SW[5],SW[4],SW[3],SW[2],SW[1],SW[0] }), 
     .operator_2({SW[15],SW[14],SW[13],SW[12],SW[11],SW[10],SW[9],SW[8]}),
     .result(result));
    
    Default_state Upp( .sw(SW), 
    .opCode({BTNU, BTNL, BTNR, BTND}), 
    .led(LED), .result(result),.control_display(cable_state)); 
    
    ////
    logic [3:0] result1, result2, result3, result4;
    logic  estado_display;
    
    control_display(.sw(SW), .result_alu(result), .control_display(cable_state),
    .result1(result1), .result2(result2), .result3(result3), .result4(result4), 
    .estado_display(estado_display)); 
     
    logic [3:0] valor0;
    logic [3:0] valor1;
    logic [3:0] valor2;
    logic [3:0] valor3;
    logic [3:0] valor4;
    logic [3:0] valor5;
    logic [3:0] valor6;
    logic [3:0] valor7;
    
    assign valor0 = result1;
    assign valor1 = result2;
    assign valor2 = 4'h0;
    assign valor3 = 4'h0;
    assign valor4 = result3;
    assign valor5 = result4;
    assign valor6 = 4'h0;
    assign valor7 = 4'h0;
    
    
    Display_8casillas utt(.CLK100MHZ(CLK100MHZ), .valor0(valor0), 
    .valor1(valor1), .valor2(valor2), .valor3(valor3), .valor4(valor4),
    .valor5(valor5), .valor6(valor6), .valor7(valor7), 
    .CA(CA), .CB(CB), .CC(CC), .CD(CD), .CE(CE), .CF(CF), 
    .CG(CG), .DP(DP), .AN(AN));
    
    
endmodule
