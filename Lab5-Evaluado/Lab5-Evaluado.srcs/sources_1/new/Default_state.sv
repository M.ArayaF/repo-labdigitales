`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/07/2019 03:15:32 AM
// Design Name: 
// Module Name: Default_state
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Default_state(
    input logic [15:0] sw,
    input logic [3:0] opCode,
    input logic [7:0] result,
    output logic [15:0] led,
    output logic [1:0] control_display
    );
    always_comb begin
        if (opCode == 4'b0001 | opCode == 4'b1000  ) begin //resta o suma
            led = 8'b0000000000000000;
            control_display =2'b10;
        end
        else if (opCode == 4'b0010 | opCode == 4'b0100) begin // or and
            led[15:8] = 8'b00000000;
            led[7:0] = result;
            control_display = 2'b01;
        end 
        else begin // Estado por defecto
            led = sw;
            control_display = 2'b00;
        end  
    end 
endmodule
