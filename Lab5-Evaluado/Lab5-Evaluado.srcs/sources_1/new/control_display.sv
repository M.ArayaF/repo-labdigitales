`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/07/2019 04:20:59 PM
// Design Name: 
// Module Name: control_display
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module control_display(
    input logic [15:0] sw,
    input logic [7:0] result_alu,
    input logic [1:0] control_display,
    output logic [3:0] result1, result2, result3, result4,
    output logic  estado_display
    );
    always_comb begin
        if(control_display == 2'b00) begin
            result1 = sw[3:0];
            result2 = sw[7:4];
            result3 = sw[11:8];
            result4 = sw[15:12];
            estado_display = 1;
        end
        else if (control_display == 2'b01) begin
            result1 = 4'd0;
            result2 = 4'd0;
            result3 = 4'd0;
            result4 = 4'd0;
            estado_display = 0;
        end
        else begin
            result1 = result_alu[3:0];
            result2 = result_alu[7:4];
            result3 = 4'd0;
            result4 = 4'd0;
            estado_display = 1;
        end
    end
    
endmodule
