`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/07/2019 05:19:06 PM
// Design Name: 
// Module Name: contador_pul
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module contador_pul(
    input logic clk,
    input logic boton,
    output logic [2:0] out = 2'd0
    );
    always_ff @(posedge clk) begin
        if (boton == 1) begin
            out = out + 1;
        end
        else
            begin
            out = out;
        end
    end
endmodule
