`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/07/2019 05:01:49 PM
// Design Name: 
// Module Name: main_4_3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module main_4_3(
    input logic BTNC, CLK100MHZ,
    output logic CA,CB,CC,CD,CE,CF,CG,
    output logic [7:0] AN,
    output logic DP
    );
    logic [2:0] out_x;
    logic freq_inter;
    
    Divisor #(1000000000) dos(.clk_in(CLK100MHZ),.clk_out(freq_inter));
    contador_pul jiro (.clk(CLK100MHZ), .boton(BTNC),.out(out_x));
    
    assign DP = 1'b1;
    assign AN = 8'b11111110;
    BCD_to_7Seg uno(.BCD_in(out_x), 
    .sevenSeg({CG,CF,CE,CD,CC,CB,CA}));
    
endmodule
