`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06.05.2019 18:24:42
// Design Name: 
// Module Name: main_3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module main_3(
    input   logic CLK100MHZ,
    input [1:0] SW,
    output logic CA,CB,CC,CD,CE,CF,CG,
    output logic [7:0] AN,
    output logic DP
    );
    
    logic clk_inter;
    logic [3:0] count;
    assign AN = 8'b11111110;
    
    Divisor #(50000000) uut(.clk_in(CLK100MHZ), .clk_out(clk_inter)); // 1 Hz
    counter_4bit uut2(.clk(clk_inter), .reset(SW[0]) ,.count(count));
    fib_rec uut3(.BCD_in (count), .fib(DP));
    BCD_to_7Seg uut4(.BCD_in(count), .sevenSeg({CG,CF,CE,CD,CC,CB,CA}));
    
    
endmodule
