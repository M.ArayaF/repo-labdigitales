`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06.05.2019 17:50:50
// Design Name: 
// Module Name: main_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module main_2(
    input   logic CLK100MHZ, 
    output logic  [3:1] JA
    );
    Divisor #(100000) uut(.clk_in(CLK100MHZ), .clk_out(JA[1])); // 500 Hz
    Divisor #(1666667) uut2(.clk_in(CLK100MHZ),.clk_out(JA[2])); //30 Hz
    Divisor #(5000000) uut3(.clk_in(CLK100MHZ),.clk_out(JA[3])); // 10 Hz
endmodule
