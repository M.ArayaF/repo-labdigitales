`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.05.2019 13:38:13
// Design Name: 
// Module Name: Display_8casillas
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Display_8casillas(
    input logic CLK100MHZ,
    input logic [3:0] valor0,
    input logic [3:0] valor1,
    input logic [3:0] valor2,
    input logic [3:0] valor3,
    input logic [3:0] valor4,
    input logic [3:0] valor5,
    input logic [3:0] valor6,
    input logic [3:0] valor7,
    input logic apagar,
    output logic CA,CB,CC,CD,CE,CF,CG,
    output logic [7:0] AN,
    output logic DP
    );
    
    logic clk_inter;
    logic [2:0] cnt_sincro;
    logic [3:0] ordenador;
    assign DP = 1'b1;
    
    Divisor #(83333) uut(.clk_in(CLK100MHZ), .clk_out(clk_inter)); // 1 Hz
    Refresh uut2(.clk(clk_inter), .out(cnt_sincro)); // 3bit
    Demux uut3(.control(cnt_sincro), .out(AN));
    
    Mux uut4(.control(cnt_sincro), .valor0(valor0), .valor1(valor1), .valor2(valor2), .valor3(valor3), .valor4(valor4), .valor5(valor5), .valor6(valor6), .valor7(valor7), .out(ordenador));
    BCD_to_7Seg UTT(.BCD_in(ordenador), .sevenSeg({CG,CF,CE,CD,CC,CB,CA}), .apagar(apagar));

endmodule
