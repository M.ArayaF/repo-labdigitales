`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.07.2019 15:50:44
// Design Name: 
// Module Name: ALU_DD
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU_DD #(
    parameter N=8)(
    input logic clk,
    input logic rst,
    input logic [1:0] OpCode,
    input logic [N-1:0] operator_1, operator_2,
    input logic [1:0] state,
    output logic [N-1:0] result,
    output logic [31:0] bcd_op1, bcd_op2, bcd_result,
    output logic [1:0] state_result
    );
    
    logic [N-1:0] result_aux, result_aux_reg;
    
    always_comb begin
        if (state == 2'b10)
            case (OpCode)
                2'b00: result_aux = operator_1 + operator_2;
                2'b01: result_aux = operator_1 - operator_2;
                2'b10: result_aux = operator_1 | operator_2;
                default: result_aux = operator_1 & operator_2;
            endcase
        else
            result_aux = result_aux_reg;
    end 
    
    always_comb begin
        if (operator_1[15] == 1'b0 && operator_2[15] == 1'b0) //ambos positivos
            if (result_aux[15] == 1'b0)
                state_result = 2'b01;  //no overflow
            else
                state_result = 2'b10;   //overlflow
        else 
            if (operator_1[15] == 1'b1 & operator_2[15] == 1'b1) // ambos negativos
                if (result_aux[15] == 1'b1)
                    state_result = 2'b01; //  no overflow 
                else 
                    state_result = 2'b10; // overflow
            else
                state_result = 2'b01; // no overflow
    end
            
    bin_to_bcd DD_Op1(.clk(clk),.trigger(1'd1),.in({16'd0,operator_1}),.bcd(bcd_op1), .idle());
    bin_to_bcd DD_Op2(.clk(clk),.trigger(1'd1),.in({16'd0,operator_2}),.bcd(bcd_op2),.idle());
    bin_to_bcd DD_res(.clk(clk),.trigger(1'd1),.in({16'd0,result_aux[N-1:0]}),.bcd(bcd_result),.idle());
    
    assign result = result_aux;
    
    always_ff @(posedge clk) begin 
        if (rst) begin
             result_aux_reg <= {N+1{1'd0}};
        end
        else begin
            result_aux_reg <= result_aux;
        end
    end
    
endmodule