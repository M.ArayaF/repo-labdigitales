`timescale 1ns / 1ps


module control_6_final(
    input logic clk,
    input logic [15:0] SW,
    input logic rst,
    input logic [4:0] controlador,
    input logic [1:0] state,
    output logic [31:0] salida,
    output logic apagar,
    output logic [1:0] led_RG
    );
    
    logic [15:0] resultado;   
    logic [31:0] registro;
    logic [15:0] operador_1, operador_2;
    logic [31:0] resultado_bcd, bcd_op1, bcd_op2;  
    logic [1:0] state_result_aux;
    
    ALU_DD #(.N(16)) Alu(
    .rst(rst),
    .clk(clk),
    .OpCode(SW[1:0]), 
    .operator_1(registro[31:16]),
    .operator_2(registro[15:0]), 
    .result(resultado),
    .state(state),
    .bcd_op1(bcd_op1),
    .bcd_op2(bcd_op2),
    .bcd_result(resultado_bcd),
    .state_result(state_result_aux));
    
    always_comb begin
        case (controlador[0])
            1'b1: operador_1 = SW;
            default operador_1 = registro[31:16];
        endcase 
    end
    
    always_comb begin
        case (controlador[1])
            1'b1:operador_2 = SW;
            default operador_2 = registro[15:0];
        endcase 
    end
    
    always_comb begin
        apagar = 1'd0;
        led_RG = 2'b00;
        
        case (controlador[4:2])
            3'b001: begin   // Mostrar 4'd0 y operador 1_hexa
                        salida[31:16] = 16'd0;
                        salida[15:0] = operador_1;
            end
            3'b010: begin   // Mostrar operador1_hexa y operador 2_hexa
                        salida[31:16] = operador_2;
                        salida[15:0] = operador_1;
            end
            3'b011: begin   // Mostrar d'0 y resultado_hexa
                        salida[31:16] = 16'd0;
                        salida[15:0] = resultado;
                        led_RG = state_result_aux;
            end
            3'b100: begin   // Mostrar operador1_deci   
                        salida = bcd_op1;
            end
            3'b101: begin   // Mostrar operador1_deci y operador2_decimal
                        salida[31:16] = bcd_op2[15:0];
                        salida[15:0] = bcd_op1[15:0];
            end
            3'b110: begin   // Mostrar resultado_deci
                        salida = resultado_bcd; 
                        led_RG = state_result_aux;
            end
            default begin   // Mostrar puros 0
                    salida = 32'd0;
                    apagar = 1'd1;
            end
        endcase 
    end

    
    always_ff @(posedge clk) begin 
        if (rst) begin
             registro <= 32'b0;
        end
        else begin
            registro <= {operador_1, operador_2};
        end
    end
endmodule