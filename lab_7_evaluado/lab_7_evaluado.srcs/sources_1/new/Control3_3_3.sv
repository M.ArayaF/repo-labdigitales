`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/24/2019 04:20:54 PM
// Design Name: 
// Module Name: Control3_3_3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:|
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Control3_3_3(
    input logic rst,
    input logic clk,
	input logic [1:0] OP1,
	input logic [1:0] OP2,
    input logic ALU_ctrl,
    input logic [7:0] rx_data,
    
    output logic [15:0] operando1_salida,
    output logic [15:0] operando2_salida,
    output logic [1:0] ALU_control_salida
    );
    
    logic [15:0] operando1;
    logic [15:0] operando2;
    logic [1:0] ALU_control;
    
    logic [15:0] operando1_next;
    logic [15:0] operando2_next;
    logic [1:0]  ALU_control_next;
    
    
    
    always_comb
            begin
                case(OP1)
                    2'b01: operando1_next = {8'd0,rx_data};
                    2'b10: operando1_next = {rx_data,operando1[7:0]};
                    default operando1_next = operando1;
                 endcase
                 case(OP2)
                    2'b01: operando2_next = {8'd0,rx_data};
                    2'b10: operando2_next = {rx_data,operando2[7:0]};
                    default operando2_next = operando2;
                    endcase
                 case(ALU_ctrl)
                    1'b1: ALU_control_next = rx_data[1:0];
                    default ALU_control_next = ALU_control;
                endcase
            end
            
     assign operando1_salida = operando1_next;
     assign operando2_salida = operando2_next;
     assign ALU_control_salida = ALU_control_next;
    
    always_ff @(posedge clk)
                            begin
                                if(rst)
                                    begin
                                        operando1 <= 16'd0;
                                        operando2 <= 16'd0;
                                        ALU_control <= 2'd0;
                                    end
                                else
                                    begin
                                        operando1 <= operando1_next;
                                        operando2 <= operando2_next;
                                        ALU_control <= ALU_control_next;
                                    end
                            end
endmodule
