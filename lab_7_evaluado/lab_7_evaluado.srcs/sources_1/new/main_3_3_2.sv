`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.07.2019 02:40:03
// Design Name: 
// Module Name: main_3_3_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module main_3_3_2(
	input  logic   CLK100MHZ,
	input  logic   CPU_RESETN,
	input  logic   UART_TXD_IN ,
	
	output logic   UART_RXD_OUT,
	output logic CA,CB,CC,CD,CE,CF,CG,DP,
    output logic [7:0] AN,
	output logic [15:0] LED,
	output logic [2:1] JA
);
	logic [7:0] rx_data;
    logic [7:0] tx_data;

	logic rx_ready;
	logic tx_start;
	logic tx_busy;
	
	logic [1:0] OP1;
	logic [1:0] OP2;
	logic ALU_ctrl;
	
	logic [3:0] estado;
	logic tx_result_start;
	
	logic [3:0] valor0;
    logic [3:0] valor1;
    logic [3:0] valor2;
    logic [3:0] valor3;
    logic [3:0] valor4;
    logic [3:0] valor5;
    logic [3:0] valor6;
    logic [3:0] valor7;
    logic apagar = 1'b0; // En alto se apaga el 7 segment
    
    logic [15:0] operando1;
	logic [15:0] operando2;
	logic [1:0]  Mux_state;
    logic [1:0]  Alu_Ctrl;
    logic [15:0] toDisplay;
    
    logic [15:0] resultado;
    logic v_envio;
	
	// M�dulo UART a 115200/8 bits datos/No paridad/1 bit stop 
	uart_basic #(
	.CLK_FREQUENCY(100_000_000),.BAUD_RATE(115200)) uart_basic_inst (
    .clk(CLK100MHZ),
    .reset(~CPU_RESETN),
    .rx(UART_TXD_IN),
    .rx_data(rx_data),
    .rx_ready(rx_ready),
    .tx(UART_RXD_OUT),
    .tx_start(tx_start),
    .tx_data(tx_data),
    .tx_busy(tx_busy)
	);
	
	UART_RX_CTRL RX_CTRL (
	.clk(CLK100MHZ),
	.rst(~CPU_RESETN),
	.rx_ready(rx_ready),
	.stateID(estado),
	.OP1(OP1),
	.OP2(OP2),
	.ALU_ctrl(ALU_ctrl),
	.tx_result_start(tx_result_start),
	.mux_controller(Mux_state)
	);
	
	Control3_3_3 guardado(
	.clk(CLK100MHZ),
	.rst(~CPU_RESETN),
	.rx_data(rx_data),
	.OP1(OP1),
	.OP2(OP2),
	.ALU_ctrl(ALU_ctrl),
	.operando1_salida(operando1),
	.operando2_salida(operando2),
	.ALU_control_salida(Alu_Ctrl)
	);
	
	ALU_mas_DisplayController CalculadoraDisplay(
	.operator_1(operando1), 
    .operator_2(operando2),
    .Alu_Ctrl(Alu_Ctrl),   
    .Mux_Crtl(Mux_state),
    .toDisplay(toDisplay), 
    .result(resultado)  
	);
	
	control3_3_4 pre_envio(
	.resultado(resultado),
	.v_envio(v_envio),
	.envio(tx_data)
	);
	
	maquina_estado_salida envio(
	.clk(CLK100MHZ),
	.rst(~CPU_RESETN),
	.tx_result_start(tx_result_start),
    .tx_busy(tx_busy),
    .v_envio(v_envio),
    .tx_start(tx_start)
	);
	
	bin_to_bcd DD_Op1(
	.clk(CLK100MHZ),
	.trigger(1'd1),
	.in({16'd0,toDisplay}),
	.bcd({valor7,valor6,valor5,valor4,valor3,valor2,valor1 ,valor0}), 
	.idle());
	
	assign JA[1] = UART_TXD_IN;
	assign JA[2] = UART_RXD_OUT; 
	
	//---------------------- Bloque 2 para hacer funcionar los 7 segmentos ----------------------         
    Display_8casillas utt(
    .CLK100MHZ(CLK100MHZ),
    .valor0(valor0), 
    .valor1(valor1),
    .valor2(valor2),
    .valor3(valor3),
    .valor4(valor4),
    .valor5(valor5),
    .valor6(valor6),
    .valor7(valor7), 
    .CA(CA),
    .CB(CB),
    .CC(CC),
    .CD(CD),
    .CE(CE),
    .CF(CF), 
    .CG(CG),
    .DP(DP),
    .AN(AN),
    .apagar(apagar));
//---------------------- Fin bloque 2 para hacer funcionar los 7 segmentos ----------------------

	
endmodule

