`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.07.2019 17:21:55
// Design Name: 
// Module Name: control3_3_4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module control3_3_4(
    input logic [15:0] resultado,
    input logic v_envio,
    output logic [7:0] envio
    );
    
    always_comb
        case(v_envio)
            1'b0: envio = resultado[7:0];
            default: envio = resultado[15:8];
        endcase
endmodule
