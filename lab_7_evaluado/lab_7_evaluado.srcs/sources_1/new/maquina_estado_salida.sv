`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.07.2019 17:54:24
// Design Name: 
// Module Name: maquina_estado_salida
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module maquina_estado_salida(
    input logic        clk,
	input logic        rst,
	input logic        tx_busy,
	input logic        tx_result_start, // Indica que hay q iniciar la transmision del resultado hacia endpoint master  
	output logic       v_envio,
	output logic       tx_start
	);
   
    enum logic [2:0] {IDLE, Resultado_B_1_Generar, Resultado_B_1_Enviar ,Resultado_B_2_Generar, Resultado_B_2_Enviar} 
    state, next_state;
    
    
    always_comb begin
        next_state = state;
        v_envio = 1'b1;
        tx_start = 1'b0;
        
        case (state)
        IDLE:                 begin
                                    if (tx_result_start)
                                        next_state = Resultado_B_1_Generar;
                                 
                                end  
        
        Resultado_B_1_Generar:  begin
                                    tx_start = 1'b1;
                                    next_state = Resultado_B_1_Enviar;
                                 
                                end
        Resultado_B_1_Enviar:   begin
                                    v_envio = 1'b0;
                                    if(tx_busy == 1'b0)
                                        next_state = Resultado_B_2_Generar;
                                end
        Resultado_B_2_Generar:  begin
                                    tx_start = 1'b1;
                                    v_envio = 1'b0;
                                    next_state = Resultado_B_2_Enviar;
                                end
        Resultado_B_2_Enviar:   begin
                                    v_envio = 1'b0;
                                    if(tx_busy  == 1'b0)
                                        next_state = IDLE;
                                end
        endcase 
    end
    
    always_ff @(posedge clk) begin
    	if(rst)
    		state <= IDLE;
    	else
    		state <= next_state;
	end
    
endmodule
