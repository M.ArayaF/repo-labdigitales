`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/25/2019 04:32:27 PM
// Design Name: 
// Module Name: ALU_mas_DisplayController
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU_mas_DisplayController(
    input logic [15:0]  operator_1, 
    input logic [15:0]  operator_2,
    input logic [1:0]   Alu_Ctrl,
    input logic [1:0]   Mux_Crtl,
    
    output logic [15:0] toDisplay,
    output logic [15:0] result
    
    );
    
    logic [31:0] bcd_op1;
    logic [31:0] bcd_op2;
    logic [31:0] bcd_result;
    
    
    Alu_3_3_3 AluToDisplay(
    .operator_1(operator_1),
    .operator_2(operator_2),
    .Alu_Ctrl(Alu_Ctrl),           
    .result(result)
    );
    
    
    
    always_comb 
                begin
                    case(Mux_Crtl)
                        2'b00: toDisplay = operator_1;                        
                        2'b01: toDisplay = operator_2;
                        2'b10: toDisplay = result;
                        default: toDisplay = 16'd0;
                    endcase
                 end
    
    
endmodule
