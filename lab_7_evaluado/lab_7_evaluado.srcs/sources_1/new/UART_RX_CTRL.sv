`timescale 1ns / 1ps

module UART_RX_CTRL(
    input logic        clk,
	input logic        rst,
	input logic        rx_ready,
	output logic [1:0] OP1,
	output logic [1:0] OP2,
	output logic       ALU_ctrl,
	output logic [3:0] stateID,
	output logic [1:0] mux_controller,
	output logic       tx_result_start // Indica que hay q iniciar la transmision del resultado hacia endpoint master
    );
   
    enum logic [3:0] {Wait_OP1_LSB, Store_OP1_LSB, Wait_OP1_MSB, Store_OP1_MSB,
    Wait_OP2_LSB, Store_OP2_LSB, Wait_OP2_MSB, Store_OP2_MSB, 
    Wait_CMD, Store_CMD, Delay_1_cycle, Trigger_TX_result} state, next_state;
    
    assign stateID = state;
    
    always_comb begin
        next_state = state;
        tx_result_start = 1'b0;
        OP1 = 2'b00;
        OP2 = 2'b00;
        ALU_ctrl = 1'b0;
        mux_controller = 2'd0;
        case (state)
        Wait_OP1_LSB:   begin
                            mux_controller = 2'b10;
                            if (rx_ready)
                                next_state = Store_OP1_LSB;
                        end
        Store_OP1_LSB:  begin
                            next_state = Wait_OP1_MSB;
                            OP1 = 2'b01;
                        end
        Wait_OP1_MSB:   begin
                            mux_controller = 2'b00;
                            if (rx_ready)
                                next_state = Store_OP1_MSB;
                        end
        Store_OP1_MSB:  begin
                            next_state = Wait_OP2_LSB;
                            OP1 = 2'b10;
                            
                        end
        Wait_OP2_LSB:   begin
                            mux_controller = 2'b00;
                            if (rx_ready)
                                next_state = Store_OP2_LSB;
                        end
        Store_OP2_LSB:  begin
                            next_state = Wait_OP2_MSB;
                            OP2 = 2'b01;
                        end
        Wait_OP2_MSB:   begin
                            mux_controller = 2'b01;
                            if (rx_ready)
                                next_state = Store_OP2_MSB;
                        end 
        Store_OP2_MSB:  begin
                            next_state = Wait_CMD;
                            OP2 = 2'b10;
                        end
        Wait_CMD:       begin
                            mux_controller = 2'b01;
                            if (rx_ready)
                                next_state = Store_CMD;
                        end 
        Store_CMD:      begin
                            next_state = Delay_1_cycle;
                            ALU_ctrl = 1'b1;
                        end
        Delay_1_cycle:  begin
                            next_state = Trigger_TX_result;
                        end
        Trigger_TX_result:  begin
                            tx_result_start = 1'b1;
                            next_state = Wait_OP1_LSB;
                            end
        endcase 
    end
    
    always_ff @(posedge clk) begin
    	if(rst)
    		state <= Wait_OP1_LSB;
    	else
    		state <= next_state;
	end
    
endmodule
