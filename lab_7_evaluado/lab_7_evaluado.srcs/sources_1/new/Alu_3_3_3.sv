`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/25/2019 04:19:34 PM
// Design Name: 
// Module Name: Alu_3_3_3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//Alu puramente combinacional

module Alu_3_3_3(

    input logic [15:0] operator_1,
    input logic [15:0] operator_2,
    input logic [1:0]  Alu_Ctrl,
    
    output logic [15:0] result 

    );
    
    always_comb 
        begin
            case (Alu_Ctrl)
                2'b00: result = operator_1 + operator_2;
                2'b01: result = operator_1 - operator_2;
                2'b10: result = operator_1 | operator_2;
                default: result = operator_1 & operator_2;
            endcase

    end 
    
    
endmodule
