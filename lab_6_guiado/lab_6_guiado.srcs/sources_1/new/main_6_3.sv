`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.06.2019 15:02:34
// Design Name: 
// Module Name: main_6_3
// 
//////////////////////////////////////////////////////////////////////////////////


module main_6_3(
    input logic CLK100MHZ,
    input logic [15:0] SW,
    input logic BTNL, BTNR, BTND,
    input logic CPU_RESETN,
    //output logic LED16_B, LED16_G, LED16_R,
    output logic CA,CB,CC,CD,CE,CF,CG,DP,
    output logic [7:0] AN
    );
        
    logic estado_i, estado_d;
    logic relase_i, relase_d;
    logic boton_i, boton_d;
    
    PB_Debouncer_FSM mq_bt_l(.clk(CLK100MHZ), .rst(~CPU_RESETN), .PB(BTNL), .PB_pressed_status(estado_i), .PB_pressed_pulse(boton_i), .PB_released_pulse(relase_i));
    PB_Debouncer_FSM mq_bt_d(.clk(CLK100MHZ), .rst(~CPU_RESETN), .PB(BTNR), .PB_pressed_status(estado_d), .PB_pressed_pulse(boton_d), .PB_released_pulse(relase_d));

//---------------------- Bloque 1 para hacer funcionar los 7 segmentos ----------------------     
    logic [3:0] valor0;
    logic [3:0] valor1;
    logic [3:0] valor2;
    logic [3:0] valor3;
    logic [3:0] valor4;
    logic [3:0] valor5;
    logic [3:0] valor6;
    logic [3:0] valor7;
//---------------------- Fin bloque 1 para hacer funcionar los 7 segmentos ----------------------
  
    control_2_3 uut3(.clk(CLK100MHZ), .SW(SW[15:0]), .rst(~CPU_RESETN), .v_control_i(boton_i), .v_control_d(boton_d), .salida({valor7,valor6,valor5,valor4,valor3,valor2,valor1 ,valor0}));

//---------------------- Bloque 2 para hacer funcionar los 7 segmentos ----------------------         
    Display_8casillas utt(.CLK100MHZ(CLK100MHZ), .valor0(valor0), 
    .valor1(valor1), .valor2(valor2), .valor3(valor3), .valor4(valor4),
    .valor5(valor5), .valor6(valor6), .valor7(valor7), 
    .CA(CA), .CB(CB), .CC(CC), .CD(CD), .CE(CE), .CF(CF), 
    .CG(CG), .DP(DP), .AN(AN));
//---------------------- Fin bloque 2 para hacer funcionar los 7 segmentos ----------------------

endmodule
