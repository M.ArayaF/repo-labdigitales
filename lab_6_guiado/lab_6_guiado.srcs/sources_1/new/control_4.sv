`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.06.2019 15:28:09
// Design Name: 
// Module Name: 
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module control_4(
    input logic [15:0] SW,
    input logic rst,
    input logic [5:0] push_buttons, // {BTNL,BTNR,BTNC,BTNU}
    input logic clk,
    output logic [31:0] display
    );
    
    logic [15:0] resultado;   
    logic [31:0] salida_ini,operadores_ini;
    logic [31:0] salida,operadores;
      
    ALU #(.N(16)) Alu(
    .OpCode(4'b1000), 
    .operator_1(salida_ini[15:0]),
    .operator_2(salida_ini[31:16]), 
    .result(resultado));
   
    assign display = salida;
    
    always_comb begin
        case (push_buttons) //{BTNL,BTNR,BTNC,BTNU state_u si es q esta presionado}
            5'b10000: //  left
                begin 
                    salida[31:16] = SW;
                    salida[15:0] = salida_ini[15:0];
                    
                    operadores[31:16] = SW;
                    operadores[15:0] = operadores_ini[15:0];
                end
            5'b01000: //rigth
                begin
                    salida[31:16] = salida_ini[31:16];
                    salida[15:0] = SW;
                    
                    operadores[31:16] = operadores_ini[31:16];
                    operadores[15:0] = SW;
                end
            5'b00100: //center
                begin
                    salida[31:16] = 16'd0;
                    salida[15:0] = resultado;
                    
                    operadores = operadores_ini;
                end
            5'b00010:
                begin
                    salida[31:16] = operadores_ini[31:16];
                    salida[15:0] = operadores_ini[15:0];
                    operadores = operadores_ini;
                end
            5'b00001:
                begin
                    salida[31:16] = 16'd0;
                    salida[15:0] = resultado;
                    operadores = operadores_ini;
                end
             default 
                begin
                    salida = salida_ini;
                    operadores = operadores_ini;
                end
        endcase
    end
    
    always_ff @(posedge clk) begin 
        if (rst) begin
             salida_ini <= 32'b0;
             operadores_ini <= 32'b0;
        end
        else begin
            salida_ini <= salida;
            operadores_ini <= operadores;
        end
    end
    
endmodule