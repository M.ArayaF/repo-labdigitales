`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 17.07.2019 11:21:22
// Design Name: 
// Module Name: main_6_5_extra
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module main_6_5_extra(
    input logic CLK100MHZ,
    input logic [15:0] SW,
    input logic BTNC, BTNU, BTNL, BTNR, BTND,
    input logic CPU_RESETN,
    output logic [1:0] LED,
    output logic CA,CB,CC,CD,CE,CF,CG,DP,
    output logic [7:0] AN
    );
        
    logic push_l, release_l, state_l;
    logic push_r, release_r, state_r;
    logic push_c, release_c, state_c;
    logic push_u, release_u, state_u;
    logic push_d, release_d, state_d;
    
    PB_Debouncer_FSM mq_bt_l(.clk(CLK100MHZ), .rst(~CPU_RESETN), .PB(BTNL), .PB_pressed_status(state_l), .PB_pressed_pulse(push_l), .PB_released_pulse(release_l));
    PB_Debouncer_FSM mq_bt_r(.clk(CLK100MHZ), .rst(~CPU_RESETN), .PB(BTNR), .PB_pressed_status(state_r), .PB_pressed_pulse(push_r), .PB_released_pulse(release_r));
    PB_Debouncer_FSM mq_bt_u(.clk(CLK100MHZ), .rst(~CPU_RESETN), .PB(BTNU), .PB_pressed_status(state_u), .PB_pressed_pulse(push_u), .PB_released_pulse(release_u));
    PB_Debouncer_FSM mq_bt_d(.clk(CLK100MHZ), .rst(~CPU_RESETN), .PB(BTND), .PB_pressed_status(state_d), .PB_pressed_pulse(push_d), .PB_released_pulse(release_d));
    PB_Debouncer_FSM mq_bt_c(.clk(CLK100MHZ), .rst(~CPU_RESETN), .PB(BTNC), .PB_pressed_status(state_c), .PB_pressed_pulse(push_c), .PB_released_pulse(release_c));

       
//---------------------- Bloque 1 para hacer funcionar los 7 segmentos ----------------------     
    logic [3:0] valor0;
    logic [3:0] valor1;
    logic [3:0] valor2;
    logic [3:0] valor3;
    logic [3:0] valor4;
    logic [3:0] valor5;
    logic [3:0] valor6;
    logic [3:0] valor7;
    
    logic [8:0] controlador;
//---------------------- Fin bloque 1 para hacer funcionar los 7 segmentos ----------------------
  
    maquina_estado_v_2 M_estado(.clk(CLK100MHZ), .rst(~CPU_RESETN), .push_buttons({push_l,push_r,push_u,push_d,push_c}),
     .LED(LED[1:0]), .controlador_next(controlador)); 
     
   control_5 cal(.clk(CLK100MHZ), .SW(SW[15:0]), .rst(~CPU_RESETN), .controlador(controlador),
     .salida({valor7,valor6,valor5,valor4,valor3,valor2,valor1 ,valor0}));
    
//---------------------- Bloque 2 para hacer funcionar los 7 segmentos ----------------------         
    Display_8casillas utt(.CLK100MHZ(CLK100MHZ), .valor0(valor0), 
    .valor1(valor1), .valor2(valor2), .valor3(valor3), .valor4(valor4),
    .valor5(valor5), .valor6(valor6), .valor7(valor7), 
    .CA(CA), .CB(CB), .CC(CC), .CD(CD), .CE(CE), .CF(CF), 
    .CG(CG), .DP(DP), .AN(AN));
//---------------------- Fin bloque 2 para hacer funcionar los 7 segmentos ----------------------

endmodule
