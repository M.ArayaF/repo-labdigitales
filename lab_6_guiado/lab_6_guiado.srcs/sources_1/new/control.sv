`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.06.2019 13:01:01
// Description: El encargo de que si el boton es presionado, devuelva el valor a mostrar en los displays
// Revision:
// Revision 0.01 - File Created
// Comments: 
// 
//////////////////////////////////////////////////////////////////////////////////


module control(
    input logic clk,
    input logic [7:0] SW,
    input logic rst,
    input logic v_control,
    output logic [7:0] display = 0
    );
    
    logic [7:0] display_inicio;
    
    always_comb begin
    
        if(rst) begin
            display = 8'd0;
        end
        
        else if(v_control) begin
             display = SW;
        end
            
        else begin
            display = display_inicio;
        end
    end
    
    always@(posedge clk) begin
        if(rst) 
            display_inicio <= 8'd0;
        else 
            display_inicio <= display;
    end
endmodule