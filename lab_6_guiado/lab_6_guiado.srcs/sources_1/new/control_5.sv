`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.07.2019 18:49:07
// Design Name: 
// Module Name: control_5
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module control_5(
    input logic clk,
    input logic [15:0] SW,
    input logic rst,
    input logic [8:0] controlador,
    output logic [31:0] salida
    );
    
    logic [15:0] resultado;   
    logic [31:0] registro;
    logic [15:0] operador_1, operador_2;
    
    always_comb begin
        case (controlador[0])
            1'b1: operador_1 = SW;
            default operador_1 = registro[31:16];
        endcase 
    end
    
    always_comb begin
        case (controlador[1])
            1'b1:operador_2 = SW;
            default operador_2 = registro[15:0];
        endcase 
    end
    
    ALU #(.N(16)) Alu(.OpCode(controlador[5:2]), .operator_1(registro[15:0]),
    .operator_2(registro[31:16]), .result(resultado));
    
    always_comb begin
        case (controlador[8:6])
            3'b001:begin 
                    salida[31:16] = 16'd0;
                    salida[15:0] = operador_1;
            end
            3'b010:begin 
                    salida[31:16] = operador_1;
                    salida[15:0] = operador_2;
            end
            3'b011:begin 
                    salida[31:16] = 16'd0;
                    salida[15:0] = resultado;
            end
            3'b100: salida = 32'haaaaaaaa;
            3'b101: salida = 32'hbbbbbbbb;
            3'b110: salida = 32'hcccccccc;
            3'b111: salida = 32'hdddddddd;
            default begin
                    salida = 32'd0;
            end
        endcase 
    end

    
    always_ff @(posedge clk) begin 
        if (rst) begin
             registro <= 32'b0;
        end
        else begin
            registro <= {operador_1, operador_2};
        end
    end
endmodule