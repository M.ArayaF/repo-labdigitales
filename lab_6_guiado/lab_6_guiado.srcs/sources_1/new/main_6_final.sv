`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.07.2019 13:42:22
// Design Name: 
// Module Name: main_6_final
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module main_6_final(
    input logic CLK100MHZ,
    input logic [15:0] SW,
    input logic BTNC, BTNU, BTND,
    input logic CPU_RESETN,
    output logic [1:0] LED,
    output logic CA,CB,CC,CD,CE,CF,CG,DP,
    output logic [7:0] AN,
    output logic LED16_R, LED16_G
    );
      
    logic push_c;
    logic push_u, release_u;
    logic push_d;
    
    PB_Debouncer_FSM mq_bt_c(.clk(CLK100MHZ), .rst(~CPU_RESETN), .PB(BTNC), .PB_pressed_status(), .PB_pressed_pulse(push_c), .PB_released_pulse());
    PB_Debouncer_FSM mq_bt_u(.clk(CLK100MHZ), .rst(~CPU_RESETN), .PB(BTNU), .PB_pressed_status(), .PB_pressed_pulse(push_u), .PB_released_pulse(release_u));
    PB_Debouncer_FSM mq_bt_d(.clk(CLK100MHZ), .rst(~CPU_RESETN), .PB(BTND), .PB_pressed_status(), .PB_pressed_pulse(push_d), .PB_released_pulse());

       
//---------------------- Bloque 1 para hacer funcionar los 7 segmentos ----------------------     
    logic [3:0] valor0;
    logic [3:0] valor1;
    logic [3:0] valor2;
    logic [3:0] valor3;
    logic [3:0] valor4;
    logic [3:0] valor5;
    logic [3:0] valor6;
    logic [3:0] valor7;
    logic apagar; // En alto se apaga el 7 segment

//---------------------- Fin bloque 1 para hacer funcionar los 7 segmentos ----------------------
    logic [4:0] controlador;
    logic [1:0] estado_actual;
    
    ME_final M_estado(
    .clk(CLK100MHZ), 
    .rst(~CPU_RESETN), 
    .push_buttons({push_u,release_u,push_d,push_c}),
    .LED(estado_actual), 
    .controlador_next(controlador)); 
     
    control_6_final cal(
    .clk(CLK100MHZ),
    .SW(SW[15:0]), 
    .rst(~CPU_RESETN), 
    .controlador(controlador),
    .state(estado_actual),
    .led_RG({LED16_R,LED16_G}),
    .salida({valor7,valor6,valor5,valor4,valor3,valor2,valor1 ,valor0}),.apagar(apagar));
    
    assign LED[1:0] = estado_actual;
//---------------------- Bloque 2 para hacer funcionar los 7 segmentos ----------------------         
    Display_8casillas utt(.CLK100MHZ(CLK100MHZ), .valor0(valor0), 
    .valor1(valor1), .valor2(valor2), .valor3(valor3), .valor4(valor4),
    .valor5(valor5), .valor6(valor6), .valor7(valor7), 
    .CA(CA), .CB(CB), .CC(CC), .CD(CD), .CE(CE), .CF(CF), 
    .CG(CG), .DP(DP), .AN(AN), .apagar(apagar));
//---------------------- Fin bloque 2 para hacer funcionar los 7 segmentos ----------------------

endmodule