`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////

module ME_final(
    input 	logic clk,
	input 	logic rst,
	input logic [3:0] push_buttons, // {push_u,release_u,push_d,push_c}
	output  logic [1:0] LED,
	output logic [4:0] controlador_next
    );
   
    logic [1:0] state, state_next;
    logic [4:0] controlador;
    
    always_comb begin
        case({state,push_buttons})  
        // salida[6:4]  -> 000 = 0 
        //              -> 001 = 0 y op_1[0]
        //              -> 010 = op_1[0] y op_2[1] en hexa
        //              -> 011 = 0 y resultado en hexa
        //              -> 100 =  op1_decimal;
        //              -> 101 =  ambos_decimal;
        //              -> 110 = resultado_decimal
        // {state_{push_u,release_u,push_d,push_c}} {salida[6:4], op_2[1], op_1[0]}
            6'b00_0001:         controlador_next = 5'b010_10;
            6'b01_0001:         controlador_next = 5'b000_00;
           
            6'b01_0010:         controlador_next = 5'b001_01;
            6'b10_0010:         controlador_next = 5'b010_10;
            6'b11_0010:         controlador_next = 5'b000_00;
            
            6'b00_1000:         controlador_next =5'b100_00;                        
            6'b01_1000:         controlador_next =5'b101_00;
            6'b11_1000:         controlador_next =5'b110_00;
            
            6'b00_0100:         controlador_next =5'b001_01;
            6'b01_0100:         controlador_next =5'b010_10;
            6'b11_0100:         controlador_next =5'b011_00;
            
            6'b10_0001:         controlador_next = 5'b011_00;
            6'b11_0001:         controlador_next = 5'b001_01;
            default controlador_next = controlador;
        endcase
    end
    
    always_comb begin
        case ({push_buttons[1:0],state})
            4'b01_00:  state_next=2'b01;
            4'b01_01:  state_next=2'b10;
            4'b01_10:  state_next=2'b11;
            4'b01_11:  state_next=2'b00;
            
            4'b10_01:  state_next=2'b00;
            4'b10_10:  state_next=2'b01;
            4'b10_11:  state_next=2'b10;
            default state_next = state;

        endcase
    end
    
    assign LED = state_next;
    
    always_ff @(posedge clk)
    begin
        if(rst) begin
            controlador <= 5'b001_01;
            state <= 2'b00;
        end
        else begin
            controlador <= controlador_next;
            state <= state_next;
       end
    end
    
endmodule