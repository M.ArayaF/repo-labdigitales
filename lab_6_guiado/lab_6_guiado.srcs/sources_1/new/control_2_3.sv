`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.06.2019 15:28:09
// Design Name: 
// Module Name: control_2_3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module control_2_3(
    input logic [15:0] SW,
    input logic rst,
    input logic v_control_i,
    input logic v_control_d,
    input logic clk,
    output logic [31:0] salida
    );
    
    logic [31:0] salida_inicio;
    
    always_ff @(posedge clk) begin 
        if (rst) begin
             salida_inicio <= 32'b0;
        end
        else begin
            salida_inicio <= salida;
        end
    end
    
    always_comb begin
        if(v_control_i) begin
            salida[31:16] = SW;
            salida[15:0] = salida_inicio[15:0];
        end
        else if(v_control_d) begin
            salida[31:16] = salida_inicio[31:16];
            salida[15:0] = SW;
        end
        else begin
            salida = salida_inicio;
        end
    end
endmodule