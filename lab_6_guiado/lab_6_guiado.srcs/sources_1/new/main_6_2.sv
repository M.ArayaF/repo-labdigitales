`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/14/2019 02:48:37 PM
// Design Name: 
// Module Name: main_6_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module main_6_2(
    input logic CLK100MHZ,
    input logic [15:0] SW,
    input logic BTNC, BTNU, BTNL, BTNR, BTND,
    input logic CPU_RESETN,
    output logic CA,CB,CC,CD,CE,CF,CG,DP,
    output logic [7:0] AN
        );
        
    logic estado;
    logic re;
    logic boton;
    

    // La maquina de estado controla el boton BTNC
    PB_Debouncer_FSM utt2(.clk(CLK100MHZ), .rst(~CPU_RESETN), .PB(BTNC), .PB_pressed_status(estado), .PB_pressed_pulse(boton), .PB_released_pulse(re));

//---------------------- Bloque 1 para hacer funcionar los 7 segmentos ----------------------   
    logic [3:0] valor0;
    logic [3:0] valor1;
    logic [3:0] valor2;
    logic [3:0] valor3;
    logic [3:0] valor4;
    logic [3:0] valor5;
    logic [3:0] valor6;
    logic [3:0] valor7;
    
    //assign valor0 = SW[3:0];
    //assign valor1 = SW[7:4];
    assign valor2 = 4'h0;
    assign valor3 = 4'h0;
    assign valor4 = 4'h0;
    assign valor5 = 4'h0;
    assign valor6 = 4'h0;
    assign valor7 = 4'h0;
//---------------------- Fin bloque 1 para hacer funcionar los 7 segmentos ----------------------
    
    control uut3(.clk(CLK100MHZ), .SW(SW[7:0]), .rst(~CPU_RESETN), .v_control(boton), .display({valor1 ,valor0}));
    
//---------------------- Bloque 2 para hacer funcionar los 7 segmentos ----------------------       
    Display_8casillas utt(.CLK100MHZ(CLK100MHZ), .valor0(valor0), 
    .valor1(valor1), .valor2(valor2), .valor3(valor3), .valor4(valor4),
    .valor5(valor5), .valor6(valor6), .valor7(valor7), 
    .CA(CA), .CB(CB), .CC(CC), .CD(CD), .CE(CE), .CF(CF), 
    .CG(CG), .DP(DP), .AN(AN));
 //---------------------- Fin bloque 2 para hacer funcionar los 7 segmentos ----------------------
endmodule
