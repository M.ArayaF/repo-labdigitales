`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 17.07.2019 11:51:29
// Design Name: 
// Module Name: maquina_estado_v_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module maquina_estado_v_2(
    input 	logic clk,
	input 	logic rst,
	input logic [4:0] push_buttons, // {BTNL,BTNR,BTNC,BTNU, BTND}
	output  logic [1:0] LED,
	output logic [8:0] controlador_next
    );
   
    logic [1:0] state, state_next;
    logic [8:0] controlador;
    
    always_comb begin
        case({state,push_buttons})
        // operacion[5:0]   -> 0001 = -
        //                  -> 0010 = |
        //                  -> 0100 = &
        //                  -> 1000 = +
        //                  -> 0000 = 0
        
        // salida[8:6]  -> 000 = 0 
        //              -> 001 = 0 y op_1[0]
        //              -> 010 = op_1[0] yop_2[1]
        //              -> 011 = 0 y resultado 
        //              -> 100 = aaaaaaaa;
        //              -> 101 = bbbbbbbb;
        //              -> 110 = cccccccc;
        //              -> 111 = dddddddd; 
        
        // {estado[6:5],{BTNL,BTNR,BTNU,BTND,BTNC}}:   {salida[8:6], operacion[5:2],  op_2[1], op_1[0]}
            7'b00_00001:                                 controlador_next= 9'b010_0000_10;
            7'b01_00001:                                 controlador_next= 9'b000_0000_00;
            
            7'b01_00010:                                 controlador_next= 9'b001_0000_01;
            7'b10_00010:                                 controlador_next= 9'b010_0000_10;
            7'b11_00010:                                 controlador_next= 9'b000_0000_00;
            
            7'b10_01000:                                 controlador_next= 9'b100_0001_00;//-
            7'b10_00100:                                 controlador_next= 9'b110_0100_00;//&
            7'b10_10000:                                 controlador_next= 9'b111_1000_00;//+
            
            7'b10_00001:                                 controlador_next= {3'b011,controlador[5:2],2'b00};
            7'b11_00001:                                 controlador_next= 9'b001_0000_01;
            default controlador_next= controlador;
        endcase
    end
    
    always_comb begin
        case ({push_buttons[1:0],state})
            4'b01_00:  state_next=2'b01;
            4'b01_01:  state_next=2'b10;
            4'b01_10:  state_next=2'b11;
            4'b01_11:  state_next=2'b00;
            
            4'b10_01:  state_next=2'b00;
            4'b10_10:  state_next=2'b01;
            4'b10_11:  state_next=2'b10;
            default state_next = state;

        endcase
    end
    
    assign LED = state_next;
    
    always_ff @(posedge clk)
    begin
        if(rst) begin
            controlador <= 9'b001_0000_01;
            state <= 2'b00;
        end
        else begin
            controlador <= controlador_next;
            state <= state_next;
       end
    end
    
endmodule