`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/06/2019 06:25:12 PM
// Design Name: 
// Module Name: Refresh
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Refresh(
    input logic clk,
    output logic [2:0] out = 2'd0
    );
    always_ff @(posedge clk) begin
        out = out + 1;
    end
endmodule