`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/04/2019 09:50:49 PM
// Design Name: 
// Module Name: ALU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU #(
    parameter N=8)(
    input logic [3:0] OpCode,
    input logic [N-1:0] operator_1, operator_2,
    output logic [N-1:0] result
    );
    always_comb begin
        case (OpCode)
            4'b0001: result = operator_1 - operator_2;
            4'b0010: result = operator_1 | operator_2;
            4'b0100: result = operator_1 & operator_2;
            4'b1000: result = operator_1 + operator_2;
            default: result={N{1'd0}};
        endcase
    end 
endmodule
