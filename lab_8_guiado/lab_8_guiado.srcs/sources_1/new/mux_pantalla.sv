`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.08.2019 17:24:21
// Design Name: 
// Module Name: mux_pantalla
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux_pantalla(
    input  logic mux_op1,
    input  logic mux_op2,
    input  logic mux_alu,
    input  logic [1:0] mux_pan_ing,
    input  logic [15:0] operando1,
    input  logic [15:0] operando2,
    input  logic [2:0] alu_reg,
    input  logic [15:0] result,
    output logic [15:0] operando1_salida,
    output logic [15:0] operando2_salida,
    output logic [15:0] pan_ing,
    output logic [2:0]  alu_reg_salida
    );
    
    /////////////////////////////////////////////////////////
    ///// mux operando1_salida /////////////////////////////
    always_comb
        case(mux_op1)
            1'b1: operando1_salida = operando1;
            default: operando1_salida = 16'd0;
        endcase
        
    /////////////////////////////////////////////////////////
    ///// mux operando2_salida /////////////////////////////
    always_comb
        case(mux_op2)
            1'b1: operando2_salida = operando2;
            default: operando2_salida = 16'd0;
        endcase
    
    /////////////////////////////////////////////////////////
    ///// mux alu_reg_salida /////////////////////////////
    always_comb
        case(mux_alu)
            1'b1: alu_reg_salida = alu_reg;
            default: alu_reg_salida = 3'd7;
        endcase
    
    /////////////////////////////////////////////////////////
    ///// mux pan_ing /////////////////////////////
    always_comb
        case(mux_pan_ing)
            2'b00: pan_ing = operando1;
            2'b01: pan_ing = operando2;
            2'b10: pan_ing = {13'd0,alu_reg};
            default: pan_ing = result;
        endcase
    
        
endmodule
