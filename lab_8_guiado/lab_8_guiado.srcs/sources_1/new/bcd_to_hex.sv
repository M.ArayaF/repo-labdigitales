`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.08.2019 11:55:35
// Design Name: 
// Module Name: bcd_to_hex
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module bcd_to_hex(
    input logic [19:0] dec,
    output logic [15:0] hex

    );    
    always_comb
        begin
    
            case (dec)
              20'h0: hex = 16'h0000;
              20'h1: hex = 16'h0001;
              20'h2: hex = 16'h0002;
              20'h3: hex = 16'h0003;
              20'h4: hex = 16'h0004;
              20'h5: hex = 16'h0005;
              20'h6: hex = 16'h0006;
              20'h7: hex = 16'h0007;
              20'h8: hex = 16'h0008;
              20'h9: hex = 16'h0009;
              20'h10: hex = 16'h000a;
              20'h11: hex = 16'h000b;
              20'h12: hex = 16'h000c;
              20'h13: hex = 16'h000d;
              20'h14: hex = 16'h000e;
              20'h15: hex = 16'h000f;
              20'h16: hex = 16'h0010;
              20'h17: hex = 16'h0011;
              20'h18: hex = 16'h0012;
              20'h19: hex = 16'h0013;
              20'h20: hex = 16'h0014;
              20'h21: hex = 16'h0015;
              20'h22: hex = 16'h0016;
              20'h23: hex = 16'h0017;
              20'h24: hex = 16'h0018;
              20'h25: hex = 16'h0019;
              20'h26: hex = 16'h001a;
              20'h27: hex = 16'h001b;
              20'h28: hex = 16'h001c;
              20'h29: hex = 16'h001d;
              20'h30: hex = 16'h001e;
              20'h31: hex = 16'h001f;
              20'h32: hex = 16'h0020;
              20'h33: hex = 16'h0021;
              20'h34: hex = 16'h0022;
              20'h35: hex = 16'h0023;
              20'h36: hex = 16'h0024;
              20'h37: hex = 16'h0025;
              20'h38: hex = 16'h0026;
              20'h39: hex = 16'h0027;
              20'h40: hex = 16'h0028;
              20'h41: hex = 16'h0029;
              20'h42: hex = 16'h002a;
              20'h43: hex = 16'h002b;
              20'h44: hex = 16'h002c;
              20'h45: hex = 16'h002d;
              20'h46: hex = 16'h002e;
              20'h47: hex = 16'h002f;
              20'h48: hex = 16'h0030;
              20'h49: hex = 16'h0031;
              20'h50: hex = 16'h0032;
              20'h51: hex = 16'h0033;
              20'h52: hex = 16'h0034;
              20'h53: hex = 16'h0035;
              20'h54: hex = 16'h0036;
              20'h55: hex = 16'h0037;
              20'h56: hex = 16'h0038;
              20'h57: hex = 16'h0039;
              20'h58: hex = 16'h003a;
              20'h59: hex = 16'h003b;
              20'h60: hex = 16'h003c;
              20'h61: hex = 16'h003d;
              20'h62: hex = 16'h003e;
              20'h63: hex = 16'h003f;
              20'h64: hex = 16'h0040;
              20'h65: hex = 16'h0041;
              20'h66: hex = 16'h0042;
              20'h67: hex = 16'h0043;
              20'h68: hex = 16'h0044;
              20'h69: hex = 16'h0045;
              20'h70: hex = 16'h0046;
              20'h71: hex = 16'h0047;
              20'h72: hex = 16'h0048;
              20'h73: hex = 16'h0049;
              20'h74: hex = 16'h004a;
              20'h75: hex = 16'h004b;
              20'h76: hex = 16'h004c;
              20'h77: hex = 16'h004d;
              20'h78: hex = 16'h004e;
              20'h79: hex = 16'h004f;
              20'h80: hex = 16'h0050;
              20'h81: hex = 16'h0051;
              20'h82: hex = 16'h0052;
              20'h83: hex = 16'h0053;
              20'h84: hex = 16'h0054;
              20'h85: hex = 16'h0055;
              20'h86: hex = 16'h0056;
              20'h87: hex = 16'h0057;
              20'h88: hex = 16'h0058;
              20'h89: hex = 16'h0059;
              20'h90: hex = 16'h005a;
              20'h91: hex = 16'h005b;
              20'h92: hex = 16'h005c;
              20'h93: hex = 16'h005d;
              20'h94: hex = 16'h005e;
              20'h95: hex = 16'h005f;
              20'h96: hex = 16'h0060;
              20'h97: hex = 16'h0061;
              20'h98: hex = 16'h0062;
              20'h99: hex = 16'h0063;
              20'h100: hex = 16'h0064;
              20'h101: hex = 16'h0065;
              20'h102: hex = 16'h0066;
              20'h103: hex = 16'h0067;
              20'h104: hex = 16'h0068;
              20'h105: hex = 16'h0069;
              20'h106: hex = 16'h006a;
              20'h107: hex = 16'h006b;
              20'h108: hex = 16'h006c;
              20'h109: hex = 16'h006d;
              20'h110: hex = 16'h006e;
              20'h111: hex = 16'h006f;
              20'h112: hex = 16'h0070;
              20'h113: hex = 16'h0071;
              20'h114: hex = 16'h0072;
              20'h115: hex = 16'h0073;
              20'h116: hex = 16'h0074;
              20'h117: hex = 16'h0075;
              20'h118: hex = 16'h0076;
              20'h119: hex = 16'h0077;
              20'h120: hex = 16'h0078;
              20'h121: hex = 16'h0079;
              20'h122: hex = 16'h007a;
              20'h123: hex = 16'h007b;
              20'h124: hex = 16'h007c;
              20'h125: hex = 16'h007d;
              20'h126: hex = 16'h007e;
              20'h127: hex = 16'h007f;
              20'h128: hex = 16'h0080;
              20'h129: hex = 16'h0081;
              20'h130: hex = 16'h0082;
              20'h131: hex = 16'h0083;
              20'h132: hex = 16'h0084;
              20'h133: hex = 16'h0085;
              20'h134: hex = 16'h0086;
              20'h135: hex = 16'h0087;
              20'h136: hex = 16'h0088;
              20'h137: hex = 16'h0089;
              20'h138: hex = 16'h008a;
              20'h139: hex = 16'h008b;
              20'h140: hex = 16'h008c;
              20'h141: hex = 16'h008d;
              20'h142: hex = 16'h008e;
              20'h143: hex = 16'h008f;
              20'h144: hex = 16'h0090;
              20'h145: hex = 16'h0091;
              20'h146: hex = 16'h0092;
              20'h147: hex = 16'h0093;
              20'h148: hex = 16'h0094;
              20'h149: hex = 16'h0095;
              20'h150: hex = 16'h0096;
              20'h151: hex = 16'h0097;
              20'h152: hex = 16'h0098;
              20'h153: hex = 16'h0099;
              20'h154: hex = 16'h009a;
              20'h155: hex = 16'h009b;
              20'h156: hex = 16'h009c;
              20'h157: hex = 16'h009d;
              20'h158: hex = 16'h009e;
              20'h159: hex = 16'h009f;
              20'h160: hex = 16'h00a0;
              20'h161: hex = 16'h00a1;
              20'h162: hex = 16'h00a2;
              20'h163: hex = 16'h00a3;
              20'h164: hex = 16'h00a4;
              20'h165: hex = 16'h00a5;
              20'h166: hex = 16'h00a6;
              20'h167: hex = 16'h00a7;
              20'h168: hex = 16'h00a8;
              20'h169: hex = 16'h00a9;
              20'h170: hex = 16'h00aa;
              20'h171: hex = 16'h00ab;
              20'h172: hex = 16'h00ac;
              20'h173: hex = 16'h00ad;
              20'h174: hex = 16'h00ae;
              20'h175: hex = 16'h00af;
              20'h176: hex = 16'h00b0;
              20'h177: hex = 16'h00b1;
              20'h178: hex = 16'h00b2;
              20'h179: hex = 16'h00b3;
              20'h180: hex = 16'h00b4;
              20'h181: hex = 16'h00b5;
              20'h182: hex = 16'h00b6;
              20'h183: hex = 16'h00b7;
              20'h184: hex = 16'h00b8;
              20'h185: hex = 16'h00b9;
              20'h186: hex = 16'h00ba;
              20'h187: hex = 16'h00bb;
              20'h188: hex = 16'h00bc;
              20'h189: hex = 16'h00bd;
              20'h190: hex = 16'h00be;
              20'h191: hex = 16'h00bf;
              20'h192: hex = 16'h00c0;
              20'h193: hex = 16'h00c1;
              20'h194: hex = 16'h00c2;
              20'h195: hex = 16'h00c3;
              20'h196: hex = 16'h00c4;
              20'h197: hex = 16'h00c5;
              20'h198: hex = 16'h00c6;
              20'h199: hex = 16'h00c7;
              20'h200: hex = 16'h00c8;
              20'h201: hex = 16'h00c9;
              20'h202: hex = 16'h00ca;
              20'h203: hex = 16'h00cb;
              20'h204: hex = 16'h00cc;
              20'h205: hex = 16'h00cd;
              20'h206: hex = 16'h00ce;
              20'h207: hex = 16'h00cf;
              20'h208: hex = 16'h00d0;
              20'h209: hex = 16'h00d1;
              20'h210: hex = 16'h00d2;
              20'h211: hex = 16'h00d3;
              20'h212: hex = 16'h00d4;
              20'h213: hex = 16'h00d5;
              20'h214: hex = 16'h00d6;
              20'h215: hex = 16'h00d7;
              20'h216: hex = 16'h00d8;
              20'h217: hex = 16'h00d9;
              20'h218: hex = 16'h00da;
              20'h219: hex = 16'h00db;
              20'h220: hex = 16'h00dc;
              20'h221: hex = 16'h00dd;
              20'h222: hex = 16'h00de;
              20'h223: hex = 16'h00df;
              20'h224: hex = 16'h00e0;
              20'h225: hex = 16'h00e1;
              20'h226: hex = 16'h00e2;
              20'h227: hex = 16'h00e3;
              20'h228: hex = 16'h00e4;
              20'h229: hex = 16'h00e5;
              20'h230: hex = 16'h00e6;
              20'h231: hex = 16'h00e7;
              20'h232: hex = 16'h00e8;
              20'h233: hex = 16'h00e9;
              20'h234: hex = 16'h00ea;
              20'h235: hex = 16'h00eb;
              20'h236: hex = 16'h00ec;
              20'h237: hex = 16'h00ed;
              20'h238: hex = 16'h00ee;
              20'h239: hex = 16'h00ef;
              20'h240: hex = 16'h00f0;
              20'h241: hex = 16'h00f1;
              20'h242: hex = 16'h00f2;
              20'h243: hex = 16'h00f3;
              20'h244: hex = 16'h00f4;
              20'h245: hex = 16'h00f5;
              20'h246: hex = 16'h00f6;
              20'h247: hex = 16'h00f7;
              20'h248: hex = 16'h00f8;
              20'h249: hex = 16'h00f9;
              20'h250: hex = 16'h00fa;
              20'h251: hex = 16'h00fb;
              20'h252: hex = 16'h00fc;
              20'h253: hex = 16'h00fd;
              20'h254: hex = 16'h00fe;
              20'h255: hex = 16'h00ff;
              20'h256: hex = 16'h0100;
              20'h257: hex = 16'h0101;
              20'h258: hex = 16'h0102;
              20'h259: hex = 16'h0103;
              20'h260: hex = 16'h0104;
              20'h261: hex = 16'h0105;
              20'h262: hex = 16'h0106;
              20'h263: hex = 16'h0107;
              20'h264: hex = 16'h0108;
              20'h265: hex = 16'h0109;
              20'h266: hex = 16'h010a;
              20'h267: hex = 16'h010b;
              20'h268: hex = 16'h010c;
              20'h269: hex = 16'h010d;
              20'h270: hex = 16'h010e;
              20'h271: hex = 16'h010f;
              20'h272: hex = 16'h0110;
              20'h273: hex = 16'h0111;
              20'h274: hex = 16'h0112;
              20'h275: hex = 16'h0113;
              20'h276: hex = 16'h0114;
              20'h277: hex = 16'h0115;
              20'h278: hex = 16'h0116;
              20'h279: hex = 16'h0117;
              20'h280: hex = 16'h0118;
              20'h281: hex = 16'h0119;
              20'h282: hex = 16'h011a;
              20'h283: hex = 16'h011b;
              20'h284: hex = 16'h011c;
              20'h285: hex = 16'h011d;
              20'h286: hex = 16'h011e;
              20'h287: hex = 16'h011f;
              20'h288: hex = 16'h0120;
              20'h289: hex = 16'h0121;
              20'h290: hex = 16'h0122;
              20'h291: hex = 16'h0123;
              20'h292: hex = 16'h0124;
              20'h293: hex = 16'h0125;
              20'h294: hex = 16'h0126;
              20'h295: hex = 16'h0127;
              20'h296: hex = 16'h0128;
              20'h297: hex = 16'h0129;
              20'h298: hex = 16'h012a;
              20'h299: hex = 16'h012b;
              20'h300: hex = 16'h012c;
              20'h301: hex = 16'h012d;
              20'h302: hex = 16'h012e;
              20'h303: hex = 16'h012f;
              20'h304: hex = 16'h0130;
              20'h305: hex = 16'h0131;
              20'h306: hex = 16'h0132;
              20'h307: hex = 16'h0133;
              20'h308: hex = 16'h0134;
              20'h309: hex = 16'h0135;
              20'h310: hex = 16'h0136;
              20'h311: hex = 16'h0137;
              20'h312: hex = 16'h0138;
              20'h313: hex = 16'h0139;
              20'h314: hex = 16'h013a;
              20'h315: hex = 16'h013b;
              20'h316: hex = 16'h013c;
              20'h317: hex = 16'h013d;
              20'h318: hex = 16'h013e;
              20'h319: hex = 16'h013f;
              20'h320: hex = 16'h0140;
              20'h321: hex = 16'h0141;
              20'h322: hex = 16'h0142;
              20'h323: hex = 16'h0143;
              20'h324: hex = 16'h0144;
              20'h325: hex = 16'h0145;
              20'h326: hex = 16'h0146;
              20'h327: hex = 16'h0147;
              20'h328: hex = 16'h0148;
              20'h329: hex = 16'h0149;
              20'h330: hex = 16'h014a;
              20'h331: hex = 16'h014b;
              20'h332: hex = 16'h014c;
              20'h333: hex = 16'h014d;
              20'h334: hex = 16'h014e;
              20'h335: hex = 16'h014f;
              20'h336: hex = 16'h0150;
              20'h337: hex = 16'h0151;
              20'h338: hex = 16'h0152;
              20'h339: hex = 16'h0153;
              20'h340: hex = 16'h0154;
              20'h341: hex = 16'h0155;
              20'h342: hex = 16'h0156;
              20'h343: hex = 16'h0157;
              20'h344: hex = 16'h0158;
              20'h345: hex = 16'h0159;
              20'h346: hex = 16'h015a;
              20'h347: hex = 16'h015b;
              20'h348: hex = 16'h015c;
              20'h349: hex = 16'h015d;
              20'h350: hex = 16'h015e;
              20'h351: hex = 16'h015f;
              20'h352: hex = 16'h0160;
              20'h353: hex = 16'h0161;
              20'h354: hex = 16'h0162;
              20'h355: hex = 16'h0163;
              20'h356: hex = 16'h0164;
              20'h357: hex = 16'h0165;
              20'h358: hex = 16'h0166;
              20'h359: hex = 16'h0167;
              20'h360: hex = 16'h0168;
              20'h361: hex = 16'h0169;
              20'h362: hex = 16'h016a;
              20'h363: hex = 16'h016b;
              20'h364: hex = 16'h016c;
              20'h365: hex = 16'h016d;
              20'h366: hex = 16'h016e;
              20'h367: hex = 16'h016f;
              20'h368: hex = 16'h0170;
              20'h369: hex = 16'h0171;
              20'h370: hex = 16'h0172;
              20'h371: hex = 16'h0173;
              20'h372: hex = 16'h0174;
              20'h373: hex = 16'h0175;
              20'h374: hex = 16'h0176;
              20'h375: hex = 16'h0177;
              20'h376: hex = 16'h0178;
              20'h377: hex = 16'h0179;
              20'h378: hex = 16'h017a;
              20'h379: hex = 16'h017b;
              20'h380: hex = 16'h017c;
              20'h381: hex = 16'h017d;
              20'h382: hex = 16'h017e;
              20'h383: hex = 16'h017f;
              20'h384: hex = 16'h0180;
              20'h385: hex = 16'h0181;
              20'h386: hex = 16'h0182;
              20'h387: hex = 16'h0183;
              20'h388: hex = 16'h0184;
              20'h389: hex = 16'h0185;
              20'h390: hex = 16'h0186;
              20'h391: hex = 16'h0187;
              20'h392: hex = 16'h0188;
              20'h393: hex = 16'h0189;
              20'h394: hex = 16'h018a;
              20'h395: hex = 16'h018b;
              20'h396: hex = 16'h018c;
              20'h397: hex = 16'h018d;
              20'h398: hex = 16'h018e;
              20'h399: hex = 16'h018f;
              20'h400: hex = 16'h0190;
              20'h401: hex = 16'h0191;
              20'h402: hex = 16'h0192;
              20'h403: hex = 16'h0193;
              20'h404: hex = 16'h0194;
              20'h405: hex = 16'h0195;
              20'h406: hex = 16'h0196;
              20'h407: hex = 16'h0197;
              20'h408: hex = 16'h0198;
              20'h409: hex = 16'h0199;
              20'h410: hex = 16'h019a;
              20'h411: hex = 16'h019b;
              20'h412: hex = 16'h019c;
              20'h413: hex = 16'h019d;
              20'h414: hex = 16'h019e;
              20'h415: hex = 16'h019f;
              20'h416: hex = 16'h01a0;
              20'h417: hex = 16'h01a1;
              20'h418: hex = 16'h01a2;
              20'h419: hex = 16'h01a3;
              20'h420: hex = 16'h01a4;
              20'h421: hex = 16'h01a5;
              20'h422: hex = 16'h01a6;
              20'h423: hex = 16'h01a7;
              20'h424: hex = 16'h01a8;
              20'h425: hex = 16'h01a9;
              20'h426: hex = 16'h01aa;
              20'h427: hex = 16'h01ab;
              20'h428: hex = 16'h01ac;
              20'h429: hex = 16'h01ad;
              20'h430: hex = 16'h01ae;
              20'h431: hex = 16'h01af;
              20'h432: hex = 16'h01b0;
              20'h433: hex = 16'h01b1;
              20'h434: hex = 16'h01b2;
              20'h435: hex = 16'h01b3;
              20'h436: hex = 16'h01b4;
              20'h437: hex = 16'h01b5;
              20'h438: hex = 16'h01b6;
              20'h439: hex = 16'h01b7;
              20'h440: hex = 16'h01b8;
              20'h441: hex = 16'h01b9;
              20'h442: hex = 16'h01ba;
              20'h443: hex = 16'h01bb;
              20'h444: hex = 16'h01bc;
              20'h445: hex = 16'h01bd;
              20'h446: hex = 16'h01be;
              20'h447: hex = 16'h01bf;
              20'h448: hex = 16'h01c0;
              20'h449: hex = 16'h01c1;
              20'h450: hex = 16'h01c2;
              20'h451: hex = 16'h01c3;
              20'h452: hex = 16'h01c4;
              20'h453: hex = 16'h01c5;
              20'h454: hex = 16'h01c6;
              20'h455: hex = 16'h01c7;
              20'h456: hex = 16'h01c8;
              20'h457: hex = 16'h01c9;
              20'h458: hex = 16'h01ca;
              20'h459: hex = 16'h01cb;
              20'h460: hex = 16'h01cc;
              20'h461: hex = 16'h01cd;
              20'h462: hex = 16'h01ce;
              20'h463: hex = 16'h01cf;
              20'h464: hex = 16'h01d0;
              20'h465: hex = 16'h01d1;
              20'h466: hex = 16'h01d2;
              20'h467: hex = 16'h01d3;
              20'h468: hex = 16'h01d4;
              20'h469: hex = 16'h01d5;
              20'h470: hex = 16'h01d6;
              20'h471: hex = 16'h01d7;
              20'h472: hex = 16'h01d8;
              20'h473: hex = 16'h01d9;
              20'h474: hex = 16'h01da;
              20'h475: hex = 16'h01db;
              20'h476: hex = 16'h01dc;
              20'h477: hex = 16'h01dd;
              20'h478: hex = 16'h01de;
              20'h479: hex = 16'h01df;
              20'h480: hex = 16'h01e0;
              20'h481: hex = 16'h01e1;
              20'h482: hex = 16'h01e2;
              20'h483: hex = 16'h01e3;
              20'h484: hex = 16'h01e4;
              20'h485: hex = 16'h01e5;
              20'h486: hex = 16'h01e6;
              20'h487: hex = 16'h01e7;
              20'h488: hex = 16'h01e8;
              20'h489: hex = 16'h01e9;
              20'h490: hex = 16'h01ea;
              20'h491: hex = 16'h01eb;
              20'h492: hex = 16'h01ec;
              20'h493: hex = 16'h01ed;
              20'h494: hex = 16'h01ee;
              20'h495: hex = 16'h01ef;
              20'h496: hex = 16'h01f0;
              20'h497: hex = 16'h01f1;
              20'h498: hex = 16'h01f2;
              20'h499: hex = 16'h01f3;
              20'h500: hex = 16'h01f4;
              20'h501: hex = 16'h01f5;
              20'h502: hex = 16'h01f6;
              20'h503: hex = 16'h01f7;
              20'h504: hex = 16'h01f8;
              20'h505: hex = 16'h01f9;
              20'h506: hex = 16'h01fa;
              20'h507: hex = 16'h01fb;
              20'h508: hex = 16'h01fc;
              20'h509: hex = 16'h01fd;
              20'h510: hex = 16'h01fe;
              20'h511: hex = 16'h01ff;
              20'h512: hex = 16'h0200;
              20'h513: hex = 16'h0201;
              20'h514: hex = 16'h0202;
              20'h515: hex = 16'h0203;
              20'h516: hex = 16'h0204;
              20'h517: hex = 16'h0205;
              20'h518: hex = 16'h0206;
              20'h519: hex = 16'h0207;
              20'h520: hex = 16'h0208;
              20'h521: hex = 16'h0209;
              20'h522: hex = 16'h020a;
              20'h523: hex = 16'h020b;
              20'h524: hex = 16'h020c;
              20'h525: hex = 16'h020d;
              20'h526: hex = 16'h020e;
              20'h527: hex = 16'h020f;
              20'h528: hex = 16'h0210;
              20'h529: hex = 16'h0211;
              20'h530: hex = 16'h0212;
              20'h531: hex = 16'h0213;
              20'h532: hex = 16'h0214;
              20'h533: hex = 16'h0215;
              20'h534: hex = 16'h0216;
              20'h535: hex = 16'h0217;
              20'h536: hex = 16'h0218;
              20'h537: hex = 16'h0219;
              20'h538: hex = 16'h021a;
              20'h539: hex = 16'h021b;
              20'h540: hex = 16'h021c;
              20'h541: hex = 16'h021d;
              20'h542: hex = 16'h021e;
              20'h543: hex = 16'h021f;
              20'h544: hex = 16'h0220;
              20'h545: hex = 16'h0221;
              20'h546: hex = 16'h0222;
              20'h547: hex = 16'h0223;
              20'h548: hex = 16'h0224;
              20'h549: hex = 16'h0225;
              20'h550: hex = 16'h0226;
              20'h551: hex = 16'h0227;
              20'h552: hex = 16'h0228;
              20'h553: hex = 16'h0229;
              20'h554: hex = 16'h022a;
              20'h555: hex = 16'h022b;
              20'h556: hex = 16'h022c;
              20'h557: hex = 16'h022d;
              20'h558: hex = 16'h022e;
              20'h559: hex = 16'h022f;
              20'h560: hex = 16'h0230;
              20'h561: hex = 16'h0231;
              20'h562: hex = 16'h0232;
              20'h563: hex = 16'h0233;
              20'h564: hex = 16'h0234;
              20'h565: hex = 16'h0235;
              20'h566: hex = 16'h0236;
              20'h567: hex = 16'h0237;
              20'h568: hex = 16'h0238;
              20'h569: hex = 16'h0239;
              20'h570: hex = 16'h023a;
              20'h571: hex = 16'h023b;
              20'h572: hex = 16'h023c;
              20'h573: hex = 16'h023d;
              20'h574: hex = 16'h023e;
              20'h575: hex = 16'h023f;
              20'h576: hex = 16'h0240;
              20'h577: hex = 16'h0241;
              20'h578: hex = 16'h0242;
              20'h579: hex = 16'h0243;
              20'h580: hex = 16'h0244;
              20'h581: hex = 16'h0245;
              20'h582: hex = 16'h0246;
              20'h583: hex = 16'h0247;
              20'h584: hex = 16'h0248;
              20'h585: hex = 16'h0249;
              20'h586: hex = 16'h024a;
              20'h587: hex = 16'h024b;
              20'h588: hex = 16'h024c;
              20'h589: hex = 16'h024d;
              20'h590: hex = 16'h024e;
              20'h591: hex = 16'h024f;
              20'h592: hex = 16'h0250;
              20'h593: hex = 16'h0251;
              20'h594: hex = 16'h0252;
              20'h595: hex = 16'h0253;
              20'h596: hex = 16'h0254;
              20'h597: hex = 16'h0255;
              20'h598: hex = 16'h0256;
              20'h599: hex = 16'h0257;
              20'h600: hex = 16'h0258;
              20'h601: hex = 16'h0259;
              20'h602: hex = 16'h025a;
              20'h603: hex = 16'h025b;
              20'h604: hex = 16'h025c;
              20'h605: hex = 16'h025d;
              20'h606: hex = 16'h025e;
              20'h607: hex = 16'h025f;
              20'h608: hex = 16'h0260;
              20'h609: hex = 16'h0261;
              20'h610: hex = 16'h0262;
              20'h611: hex = 16'h0263;
              20'h612: hex = 16'h0264;
              20'h613: hex = 16'h0265;
              20'h614: hex = 16'h0266;
              20'h615: hex = 16'h0267;
              20'h616: hex = 16'h0268;
              20'h617: hex = 16'h0269;
              20'h618: hex = 16'h026a;
              20'h619: hex = 16'h026b;
              20'h620: hex = 16'h026c;
              20'h621: hex = 16'h026d;
              20'h622: hex = 16'h026e;
              20'h623: hex = 16'h026f;
              20'h624: hex = 16'h0270;
              20'h625: hex = 16'h0271;
              20'h626: hex = 16'h0272;
              20'h627: hex = 16'h0273;
              20'h628: hex = 16'h0274;
              20'h629: hex = 16'h0275;
              20'h630: hex = 16'h0276;
              20'h631: hex = 16'h0277;
              20'h632: hex = 16'h0278;
              20'h633: hex = 16'h0279;
              20'h634: hex = 16'h027a;
              20'h635: hex = 16'h027b;
              20'h636: hex = 16'h027c;
              20'h637: hex = 16'h027d;
              20'h638: hex = 16'h027e;
              20'h639: hex = 16'h027f;
              20'h640: hex = 16'h0280;
              20'h641: hex = 16'h0281;
              20'h642: hex = 16'h0282;
              20'h643: hex = 16'h0283;
              20'h644: hex = 16'h0284;
              20'h645: hex = 16'h0285;
              20'h646: hex = 16'h0286;
              20'h647: hex = 16'h0287;
              20'h648: hex = 16'h0288;
              20'h649: hex = 16'h0289;
              20'h650: hex = 16'h028a;
              20'h651: hex = 16'h028b;
              20'h652: hex = 16'h028c;
              20'h653: hex = 16'h028d;
              20'h654: hex = 16'h028e;
              20'h655: hex = 16'h028f;
              20'h656: hex = 16'h0290;
              20'h657: hex = 16'h0291;
              20'h658: hex = 16'h0292;
              20'h659: hex = 16'h0293;
              20'h660: hex = 16'h0294;
              20'h661: hex = 16'h0295;
              20'h662: hex = 16'h0296;
              20'h663: hex = 16'h0297;
              20'h664: hex = 16'h0298;
              20'h665: hex = 16'h0299;
              20'h666: hex = 16'h029a;
              20'h667: hex = 16'h029b;
              20'h668: hex = 16'h029c;
              20'h669: hex = 16'h029d;
              20'h670: hex = 16'h029e;
              20'h671: hex = 16'h029f;
              20'h672: hex = 16'h02a0;
              20'h673: hex = 16'h02a1;
              20'h674: hex = 16'h02a2;
              20'h675: hex = 16'h02a3;
              20'h676: hex = 16'h02a4;
              20'h677: hex = 16'h02a5;
              20'h678: hex = 16'h02a6;
              20'h679: hex = 16'h02a7;
              20'h680: hex = 16'h02a8;
              20'h681: hex = 16'h02a9;
              20'h682: hex = 16'h02aa;
              20'h683: hex = 16'h02ab;
              20'h684: hex = 16'h02ac;
              20'h685: hex = 16'h02ad;
              20'h686: hex = 16'h02ae;
              20'h687: hex = 16'h02af;
              20'h688: hex = 16'h02b0;
              20'h689: hex = 16'h02b1;
              20'h690: hex = 16'h02b2;
              20'h691: hex = 16'h02b3;
              20'h692: hex = 16'h02b4;
              20'h693: hex = 16'h02b5;
              20'h694: hex = 16'h02b6;
              20'h695: hex = 16'h02b7;
              20'h696: hex = 16'h02b8;
              20'h697: hex = 16'h02b9;
              20'h698: hex = 16'h02ba;
              20'h699: hex = 16'h02bb;
              20'h700: hex = 16'h02bc;
              20'h701: hex = 16'h02bd;
              20'h702: hex = 16'h02be;
              20'h703: hex = 16'h02bf;
              20'h704: hex = 16'h02c0;
              20'h705: hex = 16'h02c1;
              20'h706: hex = 16'h02c2;
              20'h707: hex = 16'h02c3;
              20'h708: hex = 16'h02c4;
              20'h709: hex = 16'h02c5;
              20'h710: hex = 16'h02c6;
              20'h711: hex = 16'h02c7;
              20'h712: hex = 16'h02c8;
              20'h713: hex = 16'h02c9;
              20'h714: hex = 16'h02ca;
              20'h715: hex = 16'h02cb;
              20'h716: hex = 16'h02cc;
              20'h717: hex = 16'h02cd;
              20'h718: hex = 16'h02ce;
              20'h719: hex = 16'h02cf;
              20'h720: hex = 16'h02d0;
              20'h721: hex = 16'h02d1;
              20'h722: hex = 16'h02d2;
              20'h723: hex = 16'h02d3;
              20'h724: hex = 16'h02d4;
              20'h725: hex = 16'h02d5;
              20'h726: hex = 16'h02d6;
              20'h727: hex = 16'h02d7;
              20'h728: hex = 16'h02d8;
              20'h729: hex = 16'h02d9;
              20'h730: hex = 16'h02da;
              20'h731: hex = 16'h02db;
              20'h732: hex = 16'h02dc;
              20'h733: hex = 16'h02dd;
              20'h734: hex = 16'h02de;
              20'h735: hex = 16'h02df;
              20'h736: hex = 16'h02e0;
              20'h737: hex = 16'h02e1;
              20'h738: hex = 16'h02e2;
              20'h739: hex = 16'h02e3;
              20'h740: hex = 16'h02e4;
              20'h741: hex = 16'h02e5;
              20'h742: hex = 16'h02e6;
              20'h743: hex = 16'h02e7;
              20'h744: hex = 16'h02e8;
              20'h745: hex = 16'h02e9;
              20'h746: hex = 16'h02ea;
              20'h747: hex = 16'h02eb;
              20'h748: hex = 16'h02ec;
              20'h749: hex = 16'h02ed;
              20'h750: hex = 16'h02ee;
              20'h751: hex = 16'h02ef;
              20'h752: hex = 16'h02f0;
              20'h753: hex = 16'h02f1;
              20'h754: hex = 16'h02f2;
              20'h755: hex = 16'h02f3;
              20'h756: hex = 16'h02f4;
              20'h757: hex = 16'h02f5;
              20'h758: hex = 16'h02f6;
              20'h759: hex = 16'h02f7;
              20'h760: hex = 16'h02f8;
              20'h761: hex = 16'h02f9;
              20'h762: hex = 16'h02fa;
              20'h763: hex = 16'h02fb;
              20'h764: hex = 16'h02fc;
              20'h765: hex = 16'h02fd;
              20'h766: hex = 16'h02fe;
              20'h767: hex = 16'h02ff;            
              default: hex = 16'hffff;
              endcase
         end
    
endmodule
