`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.08.2019 13:10:13
// Design Name: 
// Module Name: registro
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module registro(
    input logic clk,
    input logic rst,
    input logic mode,
	input logic [1:0] OP1,
	input logic [1:0] OP2,
    input logic alu_ctr_reg,
    input logic [3:0] reg_in,
    
    output logic [15:0] operando1_salida,
    output logic [15:0] operando2_salida,
    output logic [2:0] alu_salida
    );
    
    logic [15:0] operando1;
    logic [15:0] operando2;
    logic [2:0]  alu_control;
    
    logic [15:0] operando1_next;
    logic [15:0] operando2_next;
    logic [2:0]  alu_control_next;
    
    logic [31:0] operando1_dec;
    logic [15:0] operador1_hexa;
    logic [31:0] operando2_dec;
    logic [15:0] operador2_hexa;
    
    bin_to_bcd parcial_1 (.clk(clk),.trigger(1'b1),.in({16'd0,operando1}),.idle(),.bcd(operando1_dec));
    bin_to_bcd parcial_2 (.clk(clk),.trigger(1'b1),.in({16'd0,operando2}),.idle(),.bcd(operando2_dec));
    
    fromDecToHex algo_1 (.dec({operando1_dec[15:0],reg_in}),.hex(operador1_hexa));
    fromDecToHex algo_2 (.dec({operando2_dec[15:0],reg_in}),.hex(operador2_hexa));
    
    logic [15:0] operator_1_semi_final,operator_2_semi_final;
    always_comb
         
                case(mode)
                    1'b0 :  begin //  hexa
                                operator_1_semi_final = {operando1[11:0],reg_in};
                                operator_2_semi_final = {operando2[11:0],reg_in};
                            end
                    1'b1 :  begin // decimal
                                operator_1_semi_final = operador1_hexa;
                                operator_2_semi_final = operador2_hexa;
                            end
                endcase
    always_comb       
                
                case(OP1)
                    2'b01: operando1_next = operator_1_semi_final;
                    2'b10: operando1_next = {4'd0,operando1[11:0]};
                    2'b11: operando1_next = 16'd0;
                    default operando1_next = operando1;
                 endcase
    always_comb
                 case(OP2)
                    2'b01: operando2_next = operator_2_semi_final;
                    2'b10: operando2_next = {4'd0,operando2[11:0]};
                    2'b11: operando2_next = 16'd0;
                    default operando2_next = operando2;
                    endcase
    always_comb             
                 case(alu_ctr_reg)
                    1'b1: alu_control_next = reg_in[2:0];
                    default alu_control_next = alu_control;
                endcase
                
            
            
     assign operando1_salida = operando1_next;
     assign operando2_salida = operando2_next;
     assign alu_salida = alu_control_next;
    
    always_ff @(posedge clk)
            begin
                if(rst)
                    begin
                        operando1 <= 16'd0;
                        operando2 <= 16'd0;
                       alu_control <= 3'd0;
                    end
                else
                    begin
                        operando1 <= operando1_next;
                        operando2 <= operando2_next;
                        alu_control <= alu_control_next;
                    end
            end
endmodule
