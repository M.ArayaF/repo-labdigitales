`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.08.2019 00:12:28
// Design Name: 
// Module Name: mux_hex_to_bcd
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux_hex_to_bcd(
    input logic clk,
    input  logic mode,
    input  logic [15:0] operador,
    output logic [31:0] numero
    );
    
    logic [31:0] numero_bcd;
    
    bin_to_bcd op(
	   .clk(clk),
	   .trigger(1'd1),
	   .in({16'd0,operador}),
	   .bcd(numero_bcd), 
	   .idle());
    
    always_comb
        case(mode)
            1'b1:   numero = numero_bcd; // decimal
            default: numero = {16'd0,operador}; // hexa
       endcase
       
endmodule
