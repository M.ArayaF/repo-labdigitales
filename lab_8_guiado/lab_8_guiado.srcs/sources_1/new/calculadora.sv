`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 17.08.2019 22:55:59
// Design Name: 
// Module Name: calculadora
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module calculadora(
    input logic clk,
    input logic rst, 
    input logic push_c,
    input logic mode,
    input logic [4:0] val,
    output logic [15:0] operando1_salida,
    output logic [15:0] operando2_salida,
    output logic [15:0] pan_ing,
    output logic [2:0]  alu_reg_salida
    );
    
    
    
    logic [1:0] OP1;
	logic [1:0] OP2;
	logic ALU_ctr;
	
	logic [15:0] operando1;
	logic [15:0] operando2;
	logic [15:0] operando1_alu;
	logic [15:0] operando2_alu;
	logic [2:0] alu_reg;
	logic [15:0] result;
	
	logic mux_op1;
	logic mux_op2;
	logic mux_alu;
	logic [1:0] mux_pan_ing;
    
    //////////////////////////////////
	/// maquina de estado de la calculadora /////////
    maquina_estado EM(
        .mode(mode),
        .clk(clk),
	    .rst(rst),
        .push_c(push_c),
	    .val(val),
	    .OP1(OP1),
	    .OP2(OP2),
	    .ALU_ctr(ALU_ctr),
	    .mux_op1(mux_op1),
        .mux_op2(mux_op2),
        .mux_alu(mux_alu),
        .mux_pan_ing(mux_pan_ing)
	);

	
	//////////////////////////////////
	/// guardar los valores /////////
	registro registro(
        .clk(clk),
        .rst(rst),
        .mode(mode),
	    .OP1(OP1),
	    .OP2(OP2),
	    .alu_ctr_reg(ALU_ctr),
        .reg_in(val[3:0]),
        .operando1_salida(operando1),
        .operando2_salida(operando2),
        .alu_salida(alu_reg)
    );
    
    ////////////////////////////////////
    /////// mux de de bcd or hexa //////
    
    ////////////// op1 /////////////
  /* 
    mux_bcd_hex mux_ope1(
        .clk(clk),
        .mode(mode),
        .operador(operando1),
        .numero(operando1_alu)
    );
    
    ////////////// op2 /////////////
    mux_bcd_hex mux_ope2(
        .clk(clk),
        .mode(mode),
        .operador(operando2),
        .numero(operando2_alu)
    );
    
   */ 
    ///////////////////////////
    //// realiza la operacion
    
    ALU Alu(
        .operator_1(operando1),
        .operator_2(operando2),
        .alu_ctrl(alu_reg),           
        .result(result)
    );
    
    ////////////////////////////
    // seleciona que va a mostrar
   
    mux_pantalla(
        .mux_op1(mux_op1),
        .mux_op2(mux_op2),
        .mux_alu(mux_alu),
        .mux_pan_ing(mux_pan_ing),
        .operando1(operando1),
        .operando2(operando2),
        .alu_reg(alu_reg),
        .result(result),
        .operando1_salida(operando1_salida),
        .operando2_salida(operando2_salida),
        .alu_reg_salida(alu_reg_salida),
        .pan_ing(pan_ing)
    );
    
endmodule
