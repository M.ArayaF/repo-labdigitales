`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.08.2019 14:41:35
// Design Name: 
// Module Name: armar_simbolo
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module armar_simbolo(
    input  logic [19:0] operando,
    output logic [39:0] operando_ascii
    );
    
    logic [39:0] operando_ascii_tem;
    
    ///////////// operando  /////////////
            hex_to_ascii op_1_0(.hex_num(operando[3:0]),
                                .ascii_conv(operando_ascii_tem[7:0]));
            
            hex_to_ascii op_1_1(.hex_num(operando[7:4]),
                                .ascii_conv(operando_ascii_tem[15:8]));
            
            hex_to_ascii op_1_2(.hex_num(operando[11:8]),
                                .ascii_conv(operando_ascii_tem[23:16]));
            
            hex_to_ascii op_1_3(.hex_num(operando[15:12]),
                                .ascii_conv(operando_ascii_tem[31:24]));
                                
            hex_to_ascii op_1_4(.hex_num(operando[19:16]),
                                .ascii_conv(operando_ascii_tem[39:32]));
    
    always_comb
        if(operando[19:16] == 4'h0) begin //0xxxx
            if(operando[15:12] == 4'h0) begin //00xxx
                if(operando[11:8] == 4'h0) begin //000xx
                    if(operando[7:4] == 4'h0) begin // 0000x  
                            operando_ascii[39:8] = 32'd0;
                            operando_ascii[7:0] = operando_ascii_tem[7:0];                                
                    end
                    else begin //000xx                  
                        operando_ascii[39:16] = 24'd0;
                        operando_ascii[15:0] = operando_ascii_tem[15:0];           
                    end
                end
                else begin // 0xxx
                    operando_ascii[39:24] = 16'd0;
                    operando_ascii[23:0] = operando_ascii_tem[23:0];           
                end
            end
            else begin //0xxxx
                    operando_ascii[39:32] = 8'd0;
                    operando_ascii[31:0] = operando_ascii_tem[31:0];
            end
        end
        else //xxxxx
            operando_ascii[39:0] = operando_ascii_tem[39:0];
endmodule
