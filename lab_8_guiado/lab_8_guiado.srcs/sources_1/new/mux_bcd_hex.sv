`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.08.2019 12:18:35
// Design Name: 
// Module Name: mux_bcd_hex
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux_bcd_hex(
    input logic clk,
    input  logic mode,
    input  logic [15:0] operador,
    output logic [15:0] numero
    );
    
    logic [15:0] numero_hexa;
    
    fromDecToHex decHex(
    .dec({4'd0,operador}),
    .hex(numero_hexa)
    ); 
    
    always_comb
        case(mode)
            1'b0:   numero = numero_hexa;
            default: numero = operador;
       endcase
       
endmodule
