`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.08.2019 09:51:31
// Design Name: 
// Module Name: ALU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU(
    input logic [15:0] operator_1,
    input logic [15:0] operator_2,
    input logic [2:0]  alu_ctrl,
    
    output logic [15:0] result 

    );
    
    always_comb 
        begin
            case (alu_ctrl)
                3'b000: result = operator_1 + operator_2;
                3'b001: result = operator_1 * operator_2; //*
                3'b010: result = operator_1 & operator_2; // &
                3'b100: result = operator_1 - operator_2; // -
                3'b101: result = operator_1 | operator_2; // |
                default: result = 16'd0;
            endcase

    end 

endmodule
