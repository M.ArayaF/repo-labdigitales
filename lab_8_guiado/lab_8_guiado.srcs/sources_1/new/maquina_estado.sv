`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.08.2019 14:35:53
// Design Name: 
// Module Name: maquina_estado
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module maquina_estado(
    input logic        mode,
    input logic        clk,
	input logic        rst,
	input logic        push_c,
	input logic  [4:0] val,
	output logic [1:0] OP1,
	output logic [1:0] OP2,
	output logic       ALU_ctr,
	output logic       mux_op1,
    output logic       mux_op2,
    output logic       mux_alu,
    output logic [1:0] mux_pan_ing
    );
   
    enum logic [3:0] {Wait_OP1_0, Wait_OP1_1, Wait_OP1_2, Wait_OP1_3, Wait_OP1_4, ready_OP1,
                      Wait_OP2_0, Wait_OP2_1, Wait_OP2_2, Wait_OP2_3, Wait_OP2_4,ready_OP2,
                      Wait_OPE, result} state, next_state;
    
    //assign stateID = state;
    
    always_comb begin
        next_state = state;
        
        OP1 = 2'b00;
        OP2 = 2'b00;
        
        ALU_ctr = 1'b0;
        
       mux_op1 = 1'b0;
	   mux_op2 = 1'b0;
	   mux_alu = 1'b0;
	   mux_pan_ing = 2'b00;
	   
        case (state)
        Wait_OP1_0:   begin
                            mux_pan_ing = 2'b00;
                            /////////// guardar numero ///////////////
                            if (val[4] == 1'b0 && push_c == 1'b1) begin
                                next_state = Wait_OP1_1;
                                OP1 = 2'b01;
                            end
                            /////////// exe ///////////////
                            else if (val == 5'b1_0011 && push_c == 1'b1) begin
                                next_state = Wait_OP2_0;
                            end
                        end
        Wait_OP1_1:   begin
                            mux_pan_ing = 2'b00;
                            /////////// guardar numero ///////////////
                            if (val[4] == 1'b0 && push_c == 1'b1) begin
                                next_state = Wait_OP1_2;
                                OP1 = 2'b01;
                            end
                             ///////////// clk //////////////////
                            else if(val == 5'b1_0111 &&  push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                                OP2 = 2'b11;
                            end
                            /////////// ce ///////////////
                            else if (val == 5'b1_0110 && push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                            end
                            /////////// exe ///////////////
                            else if (val == 5'b1_0011 && push_c == 1'b1) begin
                                next_state = Wait_OP2_0;
                            end
                        end
        Wait_OP1_2:   begin
                            mux_pan_ing = 2'b00;
                            /////////// guardar numero ///////////////
                            if (val[4] == 1'b0 && push_c == 1'b1) begin
                                next_state = Wait_OP1_3;
                                OP1 = 2'b01;
                            end
                             ///////////// clk //////////////////
                            else if(val == 5'b1_0111 &&  push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                                OP2 = 2'b11;
                            end
                            /////////// ce ///////////////
                            else if (val == 5'b1_0110 && push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                            end
                            /////////// exe ///////////////
                            else if (val == 5'b1_0011 && push_c == 1'b1) begin
                                next_state = Wait_OP2_0;
                            end
                        end
        Wait_OP1_3:   begin
                            mux_pan_ing = 2'b00;
                            /////////// guardar numero ///////////////
                            if (val[4] == 1'b0 && push_c == 1'b1) begin
                                
                                if(mode) begin
                                    next_state = Wait_OP1_4;
                                    OP1 = 2'b01;   
                                end
                                else begin
                                    next_state = ready_OP1;
                                    OP1 = 2'b01;
                                end
                            end
                             ///////////// clk //////////////////
                            else if(val == 5'b1_0111 &&  push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                                OP2 = 2'b11;
                            end
                            /////////// ce ///////////////
                            else if (val == 5'b1_0110 && push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                            end
                            /////////// exe ///////////////
                            else if (val == 5'b1_0011 && push_c == 1'b1) begin
                                next_state = Wait_OP2_0;
                            end
                        end
         Wait_OP1_4:   begin
                            mux_pan_ing = 2'b00;
                            /////////// guardar numero ///////////////
                            if (val[4] == 1'b0 && push_c == 1'b1) begin
                                next_state = ready_OP1;
                                OP1 = 2'b01;
                            end
                             ///////////// clk //////////////////
                            else if(val == 5'b1_0111 &&  push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                                OP2 = 2'b11;
                            end
                            /////////// ce ///////////////
                            else if (val == 5'b1_0110 && push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                            end
                            /////////// exe ///////////////
                            else if (val == 5'b1_0011 && push_c == 1'b1) begin
                                next_state = Wait_OP2_0;
                            end
                        end
        ready_OP1:   begin
                            mux_pan_ing = 2'b00;
                            /////////// exe ///////////////
                            if (val == 5'b1_0011 && push_c == 1'b1) begin
                                next_state = Wait_OP2_0;
                            end
                            /////////// ce ///////////////
                            else if (val == 5'b1_0110 && push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                            end
                             ///////////// clk //////////////////
                            else if(val == 5'b1_0111 &&  push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                                OP2 = 2'b11;
                            end
                        end
        Wait_OP2_0:   begin
                            mux_op1 = 1'b1;
                            mux_pan_ing = 2'b01;
                            /////////// guardar numero ///////////////
                            if (val[4] == 1'b0 && push_c == 1'b1) begin
                                next_state = Wait_OP2_1;
                                OP2 = 2'b01;
                            end
                             ///////////// clk //////////////////
                            else if(val == 5'b1_0111 &&  push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                                OP2 = 2'b11;
                            end
                            /////////// exe ///////////////
                            else if (val == 5'b1_0011 && push_c == 1'b1) begin
                                next_state = Wait_OPE;
                            end
                        end
        Wait_OP2_1:   begin
                            mux_op1 = 1'b1;
                            mux_pan_ing = 2'b01;
                            /////////// guardar numero ///////////////
                            if (val[4] == 1'b0 && push_c == 1'b1) begin
                                next_state = Wait_OP2_2;
                                OP2 = 2'b01;
                            end
                             ///////////// clk //////////////////
                            else if(val == 5'b1_0111 &&  push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                                OP2 = 2'b11;
                            end
                            /////////// ce ///////////////
                            else if (val == 5'b1_0110 && push_c == 1'b1) begin
                                next_state = Wait_OP2_0;
                                OP2 = 2'b11;
                            end
                            /////////// exe ///////////////
                            else if (val == 5'b1_0011 && push_c == 1'b1) begin
                                next_state = Wait_OPE;
                            end
                        end
        Wait_OP2_2:   begin
                            mux_op1 = 1'b1;
                            mux_pan_ing = 2'b01;
                            /////////// guardar numero ///////////////
                            if (val[4] == 1'b0 && push_c == 1'b1) begin
                                next_state = Wait_OP2_3;
                                OP2 = 2'b01;
                            end
                             ///////////// clk //////////////////
                            else if(val == 5'b1_0111 &&  push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                                OP2 = 2'b11;
                            end
                            /////////// ce ///////////////
                            else if (val == 5'b1_0110 && push_c == 1'b1) begin
                                next_state = Wait_OP2_0;
                                OP2 = 2'b11;
                            end
                            /////////// exe ///////////////
                            else if (val == 5'b1_0011 && push_c == 1'b1) begin
                                next_state = Wait_OPE;
                            end
                        end
        Wait_OP2_3:   begin
                            mux_op1 = 1'b1;
                            mux_pan_ing = 2'b01;
                            /////////// guardar numero ///////////////
                            if (val[4] == 1'b0 && push_c == 1'b1) begin
                            if(mode) begin
                                    next_state = Wait_OP2_4;
                                    OP2 = 2'b01;   
                                end
                                else begin
                                    next_state = ready_OP2;
                                    OP2 = 2'b01;
                                end                      
                            end
                             ///////////// clk //////////////////
                            else if(val == 5'b1_0111 &&  push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                                OP2 = 2'b11;
                            end
                            /////////// ce ///////////////
                            else if (val == 5'b1_0110 && push_c == 1'b1) begin
                                next_state = Wait_OP2_0;
                                OP2 = 2'b11;
                            end
                            /////////// exe ///////////////
                            else if (val == 5'b1_0011 && push_c == 1'b1) begin
                                next_state = Wait_OPE;
                            end
                        end
                Wait_OP2_4:   begin
                            mux_op1 = 1'b1;
                            mux_pan_ing = 2'b01;
                            /////////// guardar numero ///////////////
                            if (val[4] == 1'b0 && push_c == 1'b1) begin
                                next_state = ready_OP2;
                                OP2 = 2'b01;
                            end
                             ///////////// clk //////////////////
                            else if(val == 5'b1_0111 &&  push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                                OP2 = 2'b11;
                            end
                            /////////// ce ///////////////
                            else if (val == 5'b1_0110 && push_c == 1'b1) begin
                                next_state = Wait_OP2_0;
                                OP2 = 2'b11;
                            end
                            /////////// exe ///////////////
                            else if (val == 5'b1_0011 && push_c == 1'b1) begin
                                next_state = Wait_OPE;
                            end
                        end
        ready_OP2:   begin
                            mux_op1 = 1'b1;
                            mux_pan_ing = 2'b01;
                            /////////// exe ///////////////
                            if (val == 5'b1_0011 && push_c == 1'b1) begin
                                next_state = Wait_OPE;
                                
                            end
                            /////////// ce ///////////////
                            else if (val == 5'b1_0110 && push_c == 1'b1) begin
                                next_state = Wait_OP2_0;
                                OP2 = 2'b11;
                            end
                             ///////////// clk //////////////////
                            else if(val == 5'b1_0111 &&  push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                                OP2 = 2'b11;
                            end
                        end
        Wait_OPE:   begin
                            mux_op1 = 1'b1;
                            mux_op2 = 1'b1;
                            mux_alu = 1'b1;
                            mux_pan_ing = 2'b10;
                            /////// cambiar de operador /////////
                            if ((val == 5'b1_0000 || val == 5'b1_0100 ||
                                 val == 5'b1_0001 || val == 5'b1_0101 ||
                                 val == 5'b1_0010 )&& push_c == 1'b1) begin
                            
                                ALU_ctr = 1'b1;
                            end
                            ///////////// clk //////////////////
                            else if(val == 5'b1_0111 &&  push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                                OP2 = 2'b11;
                            end
                            /////////// exe ///////////////
                            else if (val == 5'b1_0011 && push_c == 1'b1) begin
                                next_state = result;
                                
                            end
                        end                 
       result:      begin
                            mux_op1 = 1'b1;
                            mux_op2 = 1'b1;
                            mux_alu = 1'b1;
                            mux_pan_ing = 2'b11;
                            /////////// exe o clk ///////////////
                            if ((val == 5'b1_0011 || val == 5'b1_0111) 
                                && push_c == 1'b1) begin
                                next_state = Wait_OP1_0;
                                OP1 = 2'b11;
                                OP2 = 2'b11;
                            end
                        end
        endcase 
    end
    
    always_ff @(posedge clk) begin
    	if(rst)
    		state <= Wait_OP1_0;
    	else
    		state <= next_state;
	end
    
endmodule