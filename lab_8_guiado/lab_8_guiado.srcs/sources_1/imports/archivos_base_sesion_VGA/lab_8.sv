module lab_8(
	input CLK100MHZ,
	input SW,
	input BTNC,	BTNU, BTNL, BTNR, BTND, CPU_RESETN,
	output [15:0] LED,
	output CA, CB, CC, CD, CE, CF, CG,
	output DP,
	output [7:0] AN,

	output VGA_HS,
	output VGA_VS,
	output [3:0] VGA_R,
	output [3:0] VGA_G,
	output [3:0] VGA_B
	);
	
	
	logic CLK82MHZ;
	logic rst = 0;
	logic hw_rst = ~CPU_RESETN;
	
	clk_wiz_0 inst(
		// Clock out ports  
		.clk_out1(CLK82MHZ),
		// Status and control signals               
		.reset(1'b0), 
		//.locked(locked),
		// Clock in ports
		.clk_in1(CLK100MHZ)
		);
	
	logic push_l;
    logic push_r;
    logic push_c;
    logic push_u;
    logic push_d;
    
    PB_Debouncer_FSM mq_bt_l(.clk(CLK100MHZ), .rst(~CPU_RESETN), .PB(BTNL), .PB_state(), .PB_posedge(push_l), .PB_negedge());
    PB_Debouncer_FSM mq_bt_r(.clk(CLK100MHZ), .rst(~CPU_RESETN), .PB(BTNR), .PB_state(), .PB_posedge(push_r), .PB_negedge());
    PB_Debouncer_FSM mq_bt_c(.clk(CLK100MHZ), .rst(~CPU_RESETN), .PB(BTNC), .PB_state(), .PB_posedge(push_c), .PB_negedge());
    PB_Debouncer_FSM mq_bt_u(.clk(CLK100MHZ), .rst(~CPU_RESETN), .PB(BTNU), .PB_state(), .PB_posedge(push_u), .PB_negedge());
    PB_Debouncer_FSM mq_bt_d(.clk(CLK100MHZ), .rst(~CPU_RESETN), .PB(BTND), .PB_state(), .PB_posedge(push_d), .PB_negedge());
    
	/************************* VGA ********************/
	logic [2:0] op;
	logic [2:0] pos_x;
	logic [1:0] pos_y;
	logic [15:0] op1, op2;
	logic [4:0] val;
	
	grid_cursor cursor(.clk(CLK100MHZ),
                       .rst(~CPU_RESETN),
                       .restriction(SW),
                       .dir_up(push_u),
                       .dir_down(push_d),
                       .dir_left(push_l),
                       .dir_right(push_r),
                       .pos_x_salida(pos_x),
                       .pos_y_salida(pos_y),
                       .val(val));
    
    assign LED[4:0] = val;

	calculator_screen(.clk(CLK100MHZ),
                      .clk_vga(CLK82MHZ),
                      .rst(rst),
                      .mode(SW),
                      .op(op),
                      .push_c(push_c),
                      .pos_x(pos_x),
                      .pos_y(pos_y),
                      .val(val),
                      .op1(op1),
                      .op2(op2),
                      .input_screen(16'd0),
                      .VGA_HS(VGA_HS),
                      .VGA_VS(VGA_VS),
                      .VGA_R(VGA_R),
                      .VGA_G(VGA_G),
                      .VGA_B(VGA_B));
endmodule

module alu_to_ascii(
	input [2:0] alu,
	output logic[7:0] ascii_conv
	);
	always_comb
	   case(alu)
	       3'd0: ascii_conv = "+"; //suma
	       3'd1: ascii_conv = "*"; //multiplicacion
	       3'd2: ascii_conv = "&"; //and
	       3'd4: ascii_conv = "-"; //resta
	       3'd5: ascii_conv = "|"; //or
	       default: ascii_conv = 8'd32;	   
	   endcase   
endmodule

/**
 * @brief Este modulo convierte un numero hexadecimal de 4 bits
 * en su equivalente ascii de 8 bits
 *
 * @param hex_num		Corresponde al numero que se ingresa
 * @param ascii_conv	Corresponde a la representacion ascii
 *
 */

module hex_to_ascii(
    input [3:0] hex_num,
	output logic[7:0] ascii_conv
	);
	always_comb
	   case(hex_num)
	       4'h0: ascii_conv = "0"; 
	       4'h1: ascii_conv = "1"; 
	       4'h2: ascii_conv = "2"; 
	       4'h3: ascii_conv = "3"; 
	       4'h4: ascii_conv = "4"; 
	       4'h5: ascii_conv = "5"; 
	       4'h6: ascii_conv = "6"; 
	       4'h7: ascii_conv = "7"; 
	       4'h8: ascii_conv = "8"; 
	       4'h9: ascii_conv = "9";
	       4'ha: ascii_conv = "a";
	       4'hb: ascii_conv = "b";
	       4'hc: ascii_conv = "c";
	       4'hd: ascii_conv = "d";
	       4'he: ascii_conv = "e";
	       4'hf: ascii_conv = "f";  
	       default: ascii_conv = 8'd32;	   
	   endcase
endmodule

/**
 * @brief Este modulo convierte un numero hexadecimal de 4 bits
 * en su equivalente ascii, pero binario, es decir,
 * si el numero ingresado es 4'hA, la salida debera sera la concatenacion
 * del string "1010" (cada caracter del string genera 8 bits).
 *
 * @param num		Corresponde al numero que se ingresa
 * @param bit_ascii	Corresponde a la representacion ascii pero del binario.
 *
 */
module hex_to_bit_ascii(
	input [3:0]num,
	output logic [4*8-1:0]bit_ascii
	);

	always_comb
	   case(num)
	       4'h0: bit_ascii = {"0","0","0","0"};
	       4'h1: bit_ascii = {"0","0","0","1"};
	       4'h2: bit_ascii = {"0","0","1","0"};
	       4'h3: bit_ascii = {"0","0","1","1"};
	       4'h4: bit_ascii = {"0","1","0","0"};
	       4'h5: bit_ascii = {"0","1","0","1"};
	       4'h6: bit_ascii = {"0","1","1","0"};
	       4'h7: bit_ascii = {"0","1","1","1"};
	       4'h8: bit_ascii = {"1","0","0","0"};
	       4'h9: bit_ascii = {"1","0","0","1"};
	       4'ha: bit_ascii = {"1","0","1","0"};
	       4'hb: bit_ascii = {"1","0","1","1"};
	       4'hc: bit_ascii = {"1","1","0","0"};
	       4'hd: bit_ascii = {"1","1","0","1"};
	       4'he: bit_ascii = {"1","1","1","0"};
	       4'hf: bit_ascii = {"1","1","1","1"};
	       default: bit_ascii = {"0","0","0","0"};
	   endcase
endmodule

/**
 * @brief Este modulo es el encargado de dibujar en pantalla
 * la calculadora y todos sus componentes graficos
 *
 * @param clk_vga		:Corresponde al reloj con que funciona el VGA.
 * @param rst			:Corresponde al reset de todos los registros
 * @param mode			:'0' si se esta operando en decimal, '1' si esta operando hexadecimal
 * @param op			:La operacion matematica a realizar
 * @param pos_x			:Corresponde a la posicion X del cursor dentro de la grilla.
 * @param pos_y			:Corresponde a la posicion Y del cursor dentro de la grilla.
 * @param op1			:El operando 1 en formato hexadecimal.
 * @param op2			;El operando 2 en formato hexadecimal.
 * @param input_screen	:Lo que se debe mostrar en la pantalla de ingreso de la calculadora (en hexa)
 * @param VGA_HS		:Sincronismo Horizontal para el monitor VGA
 * @param VGA_VS		:Sincronismo Vertical para el monitor VGA
 * @param VGA_R			:Color Rojo para la pantalla VGA
 * @param VGA_G			:Color Verde para la pantalla VGA
 * @param VGA_B			:Color Azul para la pantalla VGA
 */
 
module calculator_screen(
    input clk,
	input clk_vga,
	input rst,
	input mode, //bcd or dec.
	input [2:0]op,
	input push_c,
	input [2:0]pos_x,
	input [1:0]pos_y,
	input [4:0] val,
	input [15:0] op1,
	input [15:0] op2,
	input [15:0] input_screen,
	
	output VGA_HS,
	output VGA_VS,
	output [3:0] VGA_R,
	output [3:0] VGA_G,
	output [3:0] VGA_B
	);
	
	localparam CUADRILLA_XI = 		212;
	localparam CUADRILLA_XF = 		CUADRILLA_XI + 600;
	
	localparam CUADRILLA_YI = 		250;
	localparam CUADRILLA_YF = 		CUADRILLA_YI + 400;
	
	logic [10:0]vc_visible,hc_visible;
	
	// MODIFICAR ESTO PARA HACER LLAMADO POR NOMBRE DE PUERTO, NO POR ORDEN!!!!!
	driver_vga_1024x768 m_driver(clk_vga, VGA_HS, VGA_VS, hc_visible, vc_visible);
	/*************************** VGA DISPLAY ************************/
		
	logic [10:0]hc_template, vc_template;
	logic [2:0]matrix_x;
	logic [1:0]matrix_y;
	logic lines;
	
	template_6x4_600x400 #(.GRID_XI(CUADRILLA_XI), 
						   .GRID_XF(CUADRILLA_XF), 
						   .GRID_YI(CUADRILLA_YI), 
						   .GRID_YF(CUADRILLA_YF)) 
    // MODIFICAR ESTO PARA HACER LLAMADO POR NOMBRE DE PUERTO, NO POR ORDEN!!!!!
	template_1(clk_vga, hc_visible, vc_visible, matrix_x, matrix_y, lines);
	
	logic [11:0]VGA_COLOR;
	
	logic text_sqrt_fg;
	logic text_sqrt_bg;

	logic [1:0]generic_fg;
	logic [1:0]generic_bg;	
	
	localparam FIRST_SQRT_X = 212;
	localparam FIRST_SQRT_Y = 250;
	
	localparam GRID_X_OFFSET	= 20;
	localparam GRID_Y_OFFSET	= 10;

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
/// simbolo dentro de la tabla
logic pintar_1;
logic pintar_2;

logic bg_0,bg_1,bg_2,bg_3,bg_s,bg_r,
      bg_4,bg_5,bg_6,bg_7,bg_m,bg_o,
      bg_8,bg_9,bg_a,bg_b,bg_u,bg_t,
      bg_c,bg_d,bg_e,bg_f,bg_x,bg_l;
      
logic fg_0,fg_1,fg_2,fg_3,fg_s,fg_r,
      fg_4,fg_5,fg_6,fg_7,fg_m,fg_o,
      fg_8,fg_9,fg_a,fg_b,fg_u,fg_t,
      fg_c,fg_d,fg_e,fg_f,fg_x,fg_l;
	
	show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*0 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*0 + GRID_Y_OFFSET)) 
                ch_00(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("0"), 
                    .in_square(bg_0), 
                    .in_character(fg_0));
		  
	show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*1 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*0 + GRID_Y_OFFSET)) 
                ch_01(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("1"), 
                    .in_square(bg_1), 
                    .in_character(fg_1));
    
    show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*2 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*0 + GRID_Y_OFFSET)) 
                ch_02(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("2"), 
                    .in_square(bg_2), 
                    .in_character(fg_2));
	
	show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*3 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*0 + GRID_Y_OFFSET)) 
                ch_03(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("3"), 
                    .in_square(bg_3), 
                    .in_character(fg_3));

    show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*4 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*0 + GRID_Y_OFFSET)) 
                ch_0s(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("+"), 
                    .in_square(bg_s), 
                    .in_character(fg_s));

	show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*5 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*0 + GRID_Y_OFFSET)) 
                ch_0r(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("-"), 
                    .in_square(bg_r), 
                    .in_character(fg_r));

	show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*0 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*1 + GRID_Y_OFFSET)) 
                ch_04(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("4"), 
                    .in_square(bg_4), 
                    .in_character(fg_4));

	show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*1 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*1 + GRID_Y_OFFSET)) 
                ch_05(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("5"), 
                    .in_square(bg_5), 
                    .in_character(fg_5));

    show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*2 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*1 + GRID_Y_OFFSET)) 	
                ch_06(  
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("6"), 
                    .in_square(bg_6), 
                    .in_character(fg_6));

	show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*3 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*1 + GRID_Y_OFFSET)) 	
                ch_07(.clk(clk_vga), 
                      .rst(rst), 
                      .hc_visible(hc_visible), 
                      .vc_visible(vc_visible), 
                      .the_char("7"), 
                      .in_square(bg_7), 
                      .in_character(fg_7));
    
    show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*4 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*1 + GRID_Y_OFFSET)) 
                ch_0m(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("*"), 
                    .in_square(bg_m), 
                    .in_character(fg_m));

	show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*5 + GRID_X_OFFSET), 
                    .CHAR_Y_LOC(FIRST_SQRT_Y + 100*1 + GRID_Y_OFFSET)) 
                ch_0o(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("|"), 
                    .in_square(bg_o), 
                    .in_character(fg_o));
    
    show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*0 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*2 + GRID_Y_OFFSET)) 
                ch_08(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("8"), 
                    .in_square(bg_8), 
                    .in_character(fg_8));
		  
	show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*1 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*2 + GRID_Y_OFFSET)) 
                ch_09(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("9"), 
                    .in_square(bg_9), 
                    .in_character(fg_9));

    show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*2 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*2 + GRID_Y_OFFSET)) 
                ch_0a(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("a"), 
                    .in_square(bg_a), 
                    .in_character(fg_a));
	
	show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*3 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*2 + GRID_Y_OFFSET)) 
                ch_0b(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("b"), 
                    .in_square(bg_b), 
                    .in_character(fg_b));
    
    show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*4 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*2 + GRID_Y_OFFSET)) 
                ch_0u(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("&"), 
                    .in_square(bg_u), 
                    .in_character(fg_u));
	
	show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*5 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*2 + GRID_Y_OFFSET)) 	
                ch_0t(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("X"), // X = CE 
                    .in_square(bg_t), 
                    .in_character(fg_t));
	
	show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*0 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*3 + GRID_Y_OFFSET)) 
                ch_0c(  
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("c"), 
                    .in_square(bg_c), 
                    .in_character(fg_c));

	show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*1 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*3 + GRID_Y_OFFSET)) 
                ch_0d(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("d"), 
                    .in_square(bg_d), 
                    .in_character(fg_d));

    show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*2 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*3 + GRID_Y_OFFSET)) 
                ch_0e(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("e"), 
                    .in_square(bg_e), 
                    .in_character(fg_e));

	show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*3 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*3 + GRID_Y_OFFSET)) 
                ch_0f(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("f"), 
                    .in_square(bg_f), 
                    .in_character(fg_f));
        
    show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*4 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*3 + GRID_Y_OFFSET)) 
                ch_0x(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("Z"), // Z = Exe
                    .in_square(bg_x), 
                    .in_character(fg_x));
	
	show_one_char #(.CHAR_X_LOC(FIRST_SQRT_X + 100*5 + GRID_X_OFFSET), 
					.CHAR_Y_LOC(FIRST_SQRT_Y + 100*3 + GRID_Y_OFFSET)) 
                ch_0l(
                    .clk(clk_vga), 
                    .rst(rst), 
                    .hc_visible(hc_visible), 
                    .vc_visible(vc_visible), 
                    .the_char("Y"), // y = clear 
                    .in_square(bg_l), 
                    .in_character(fg_l));

    assign generic_bg[0] = (bg_0 || bg_1 || bg_2 || bg_3 || bg_s || bg_r ||
                            bg_4 || bg_5 || bg_6 || bg_7 || bg_m || bg_o ||
                            bg_8 || bg_9 || bg_a || bg_b || bg_u || bg_t ||
                            bg_c || bg_d || bg_e || bg_f || bg_x || bg_l);
    
    assign generic_fg[0] = (fg_0 || fg_1 || fg_2 || fg_3 || fg_s || fg_r ||
                            fg_4 || fg_5 || fg_6 || fg_7 || fg_m || fg_o ||
                            fg_8 || fg_9 || fg_a || fg_b || fg_u || fg_t ||
                            fg_c || fg_d || fg_e || fg_f || fg_x || fg_l);
                            
     assign pintar_1 =  (bg_a || bg_b || bg_c || bg_d || bg_e || bg_f );
     assign pintar_2 =  (fg_a || fg_b || fg_c || fg_d || fg_e || fg_f );
          

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

    logic [15:0] operando1;
    logic [15:0] operando2;
    logic [15:0] pan_ing;
    logic [2:0] alu_ctr;
    
    logic [39:0] operando1_ascii;
    logic [39:0] operando2_ascii;
    logic [39:0] pan_ing_ascii;
    logic [7:0] alu_ctr_ascii;
    
    logic [31:0] operando1_final;
    logic [31:0] operando2_final;
    logic [31:0] pan_ing_final;
    
    logic [127:0] operando_b_ascii;

	calculadora calcular(
	   .clk(clk),
	   .rst(rst),
       .push_c(push_c),
	   .val(val),
	   .mode(mode),
	   .operando1_salida(operando1),
       .operando2_salida(operando2),
       .alu_reg_salida(alu_ctr),
       .pan_ing(pan_ing));
       
       //////////////////////////////////////////////////////
       
    mux_hex_to_bcd hexToBcd1(
    .clk(clk),
    .mode(mode),
    .operador(operando1),
    .numero(operando1_final)
    );
       
    mux_hex_to_bcd hexToBcd2(
    .clk(clk),
    .mode(mode),
    .operador(operando2),
    .numero(operando2_final)
    );
    
    mux_hex_to_bcd hexToBcd3(
    .clk(clk),
    .mode(mode),
    .operador(pan_ing),
    .numero(pan_ing_final)
    );
       
    armar_simbolo op_1_0(.operando(operando1_final),
                        .operando_ascii(operando1_ascii));
    armar_simbolo op_2_0(.operando(operando2_final),
                        .operando_ascii(operando2_ascii));
    armar_simbolo op_pan_ing_0(.operando(pan_ing_final),
                        .operando_ascii(pan_ing_ascii));
	   
    ///////////// operado ///////////// 
    alu_to_ascii op_alu(.alu(alu_ctr),
	                    .ascii_conv(alu_ctr_ascii));
	            
	////////////////////// operador binario //////////////////////////
	
	hex_to_bit_ascii op_b_0(.num(pan_ing[3:0]),
	                        .bit_ascii(operando_b_ascii[31:0]));
	
	hex_to_bit_ascii op_b_1(.num(pan_ing[7:4]),
	                        .bit_ascii(operando_b_ascii[63:32]));
	
	hex_to_bit_ascii op_b_2(.num(pan_ing[11:8]),
	                        .bit_ascii(operando_b_ascii[95:64]));
    
    hex_to_bit_ascii op_b_3(.num(pan_ing[15:12]),
	                        .bit_ascii(operando_b_ascii[127:96]));
	                        
	//////////////////////////////////////////////////////////////////////////
	//////////// selecionar lugar en la pantalla de los operadores //////////
	
	logic  op1_bg      , op1_fg;       // operando 1
	logic  op2_bg      , op2_fg;       // operando 2
	logic  op_alu_bg   , op_alu_fg;    // operador
	logic  pan_ing_bg  , pan_ing_fg;   // pantalla de ingreso
	logic  [3:0] op_b_bg;  // operando binario
	logic  [3:0] op_b_fg;   
	
	///////////// operando 1 /////////////
	
	show_one_line #(.LINE_X_LOCATION(FIRST_SQRT_X + 100*1 + GRID_X_OFFSET - 25), 
					.LINE_Y_LOCATION(FIRST_SQRT_Y - 100*2 + GRID_Y_OFFSET + 10), 
					.MAX_CHARACTER_LINE(5), 
					.ancho_pixel(5), 
					.n(3)) 
                show_op1(	 
                 .clk(clk_vga), 
                 .rst(rst), 
			     .hc_visible(hc_visible), 
			     .vc_visible(vc_visible), 
			     .the_line(operando1_ascii), 
			     .in_square(op1_bg), 
			     .in_character(op1_fg));
    
    ///////////// operando 2 /////////////
	
	show_one_line #(.LINE_X_LOCATION(FIRST_SQRT_X + 100*4 + GRID_X_OFFSET - 25), 
					.LINE_Y_LOCATION(FIRST_SQRT_Y - 100*2 + GRID_Y_OFFSET + 10), 
					.MAX_CHARACTER_LINE(5), 
					.ancho_pixel(5), 
					.n(3)) 
                show_op2(	 
                    .clk(clk_vga), 
			        .rst(rst), 
			        .hc_visible(hc_visible), 
			        .vc_visible(vc_visible), 
                    .the_line(operando2_ascii), 
                    .in_square(op2_bg), 
                    .in_character(op2_fg));
			     
    
    
    ///////////// operador /////////////
	
	show_one_line #(.LINE_X_LOCATION(FIRST_SQRT_X + 100*3 + GRID_X_OFFSET - 25), 
					.LINE_Y_LOCATION(FIRST_SQRT_Y - 100*2 + GRID_Y_OFFSET + 10), 
					.MAX_CHARACTER_LINE(1), 
					.ancho_pixel(5), 
					.n(3)) 
                show_op_alu( 
                    .clk(clk_vga), 
			        .rst(rst), 
			        .hc_visible(hc_visible), 
			        .vc_visible(vc_visible), 
			        .the_line(alu_ctr_ascii), 
			        .in_square(op_alu_bg), 
			        .in_character(op_alu_fg));
			     
	///////////// pantalla de ingreso /////////////
	
	show_one_line #(.LINE_X_LOCATION(FIRST_SQRT_X + 100*0 + GRID_X_OFFSET - 20), 
					.LINE_Y_LOCATION(FIRST_SQRT_Y - 100*1 + GRID_Y_OFFSET + 10), 
					.MAX_CHARACTER_LINE(5), 
					.ancho_pixel(5), 
					.n(3)) 
                show_pan_ing(
                    .clk(clk_vga), 
			        .rst(rst), 
			        .hc_visible(hc_visible), 
			        .vc_visible(vc_visible), 
			        .the_line(pan_ing_ascii), 
			        .in_square(pan_ing_bg), 
			        .in_character(pan_ing_fg));
			     
	 ///////////// operando binario /////////////
	
	show_one_line #(.LINE_X_LOCATION(FIRST_SQRT_X + 100*5 + GRID_X_OFFSET +65), 
					.LINE_Y_LOCATION(FIRST_SQRT_Y - 100*1 + GRID_Y_OFFSET + 10), 
					.MAX_CHARACTER_LINE(4), 
					.ancho_pixel(5), 
					.n(3)) 
                show_op_b_0(	
                    .clk(clk_vga), 
			        .rst(rst), 
			        .hc_visible(hc_visible), 
			        .vc_visible(vc_visible), 
			        .the_line(operando_b_ascii[31:0]), 
			        .in_square(op_b_bg[0]), 
			        .in_character(op_b_fg[0]));
	
	show_one_line #(.LINE_X_LOCATION(FIRST_SQRT_X + 100*4 + GRID_X_OFFSET + 35), 
					.LINE_Y_LOCATION(FIRST_SQRT_Y - 100*1 + GRID_Y_OFFSET + 10), 
					.MAX_CHARACTER_LINE(4), 
					.ancho_pixel(5), 
					.n(3)) 
                show_op_b_1(
					.clk(clk_vga), 
                    .rst(rst), 
			        .hc_visible(hc_visible), 
			        .vc_visible(vc_visible), 
			        .the_line(operando_b_ascii[63:32]), 
			        .in_square(op_b_bg[1]), 
			        .in_character(op_b_fg[1]));
			     
	show_one_line #(.LINE_X_LOCATION(FIRST_SQRT_X + 100*3 + GRID_X_OFFSET+5), 
					.LINE_Y_LOCATION(FIRST_SQRT_Y - 100*1 + GRID_Y_OFFSET + 10), 
					.MAX_CHARACTER_LINE(4), 
					.ancho_pixel(5), 
					.n(3)) 
                show_op_b_2(	 
                    .clk(clk_vga), 
			        .rst(rst), 
			        .hc_visible(hc_visible), 
			        .vc_visible(vc_visible), 
			        .the_line(operando_b_ascii[95:64]), 
			        .in_square(op_b_bg[2]), 
			        .in_character(op_b_fg[2]));
		     
    show_one_line #(.LINE_X_LOCATION(FIRST_SQRT_X + 100*2 + GRID_X_OFFSET - 25), 
					.LINE_Y_LOCATION(FIRST_SQRT_Y - 100*1 + GRID_Y_OFFSET + 10), 
					.MAX_CHARACTER_LINE(4), 
					.ancho_pixel(5), 
					.n(3)) 
                show_op_b_3(	 
                    .clk(clk_vga), 
			        .rst(rst), 
			        .hc_visible(hc_visible), 
			        .vc_visible(vc_visible), 
			        .the_line(operando_b_ascii[127:96]), 
			        .in_square(op_b_bg[3]), 
			        .in_character(op_b_fg[3]));
     

	hello_world_text_square m_hw(.clk(clk_vga), 
                                 .rst(1'b0),
                                 .mode(mode), 
                                 .hc_visible(hc_visible), 
                                 .vc_visible(vc_visible), 
                                 .in_square(text_sqrt_bg), 
                                 .in_character(text_sqrt_fg));
                                
     	
	assign generic_bg[1] = (op1_bg || op2_bg || op_alu_bg || pan_ing_bg || op_b_bg); 
	assign generic_fg[1] = (op1_fg || op2_fg || op_alu_fg || pan_ing_fg || op_b_fg);
	
	logic draw_cursor = (pos_x == matrix_x) && (pos_y == matrix_y);
	
	localparam COLOR_BLUE 		= 12'h00F;
	localparam COLOR_YELLOW 	= 12'hFF0; //
	localparam COLOR_RED		= 12'hF00;
	localparam COLOR_BLACK		= 12'h000;
	localparam COLOR_WHITE		= 12'hFFF;
	localparam COLOR_CYAN		= 12'h0FF;
	localparam COLOR_1		= 12'h0F4;
	localparam COLOR_2		= 12'h6F4;
	
	always@(*)
		if((hc_visible != 0) && (vc_visible != 0))
		begin		
			if(text_sqrt_fg)
				VGA_COLOR = COLOR_RED;
			else if (text_sqrt_bg)
				VGA_COLOR = COLOR_YELLOW;
				
			else if (pintar_2 && mode)
			     VGA_COLOR = COLOR_2;
			else if (pintar_1 && mode)
			     VGA_COLOR = COLOR_1;
			else if(|generic_fg)
				VGA_COLOR = COLOR_BLACK;
			else if(generic_bg)
				VGA_COLOR = COLOR_WHITE;
			
			//si esta dentro de la grilla.
			else if((hc_visible > CUADRILLA_XI) && (hc_visible <= CUADRILLA_XF) && (vc_visible > CUADRILLA_YI) && (vc_visible <= CUADRILLA_YF))
				if(lines)//lineas negras de la grilla
					VGA_COLOR = COLOR_BLACK;
				else if (draw_cursor) //el cursor
					VGA_COLOR = COLOR_CYAN;
				else
					VGA_COLOR = {3'h7, {2'b0, matrix_x} + {3'b00, matrix_y}, 4'h3};// el fondo de la grilla.
			else
				VGA_COLOR = {3'h7, {2'b0, hc_visible} + {3'b00, vc_visible}, 4'h3};//el fondo de la pantalla
		end
		else
			VGA_COLOR = COLOR_BLACK;//esto es necesario para no poner en riesgo la pantalla.

	assign {VGA_R, VGA_G, VGA_B} = VGA_COLOR;
endmodule

/**
 * @brief Este modulo cambia los ceros a la izquierda de un numero, por espacios
 * @param value			:Corresponde al valor (en hexa o decimal) al que se le desea hacer el padding.
 * @param no_pading		:Corresponde al equivalente ascii del value includos los ceros a la izquierda
 * @param padding		:Corresponde al equivalente ascii del value, pero sin los ceros a la izquierda.
 */

module space_padding(
	input [19:0] value,
	input [8*6 -1:0]no_pading,
	
	output logic [8*6 -1:0]padding);
	
	
endmodule
