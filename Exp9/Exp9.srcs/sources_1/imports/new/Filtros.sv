`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.07.2019 23:58:26
// Design Name: 
// Module Name: Filtros
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Dithering #(parameter n=8)(
    input [3*n-1:0] color_in,
    input [1:0] countx,     //  posicion x en la matriz
    input [1:0] county,     //  posicion y en la matriz
    input SW,
    output [23:0] color_out);
    
    bit [5:0][7:0]M_1;
    bit [5:0][7:0]M_2;
    bit [5:0][7:0]M_3;
    bit [5:0][7:0]M_4;
    
    logic [n-1:0] red, blue, green;
    logic [3:0] out_red, out_blue, out_green; 
    
    logic [3:0] m_value;
    
    assign red = color_in[3*n-1:3*n-8];
    assign green = color_in[2*n-1:2*n-8];
    assign blue = color_in[n-1:n-8];
    assign color_out = {out_red, red[n-5:n-8], out_green, green[n-5:n-8], out_blue, blue[n-5:n-8]};
    
    initial begin         // cambiar a always_ff si no funciona
        M_1[0] = 'd0;
        M_1[1] = 'd8;
        M_1[2] = 'd2;
        M_1[3] = 'd10;
        M_2[0] = 'd12;
        M_2[1] = 'd4;
        M_2[2] = 'd14;
        M_2[3] = 'd6;
        M_3[0] = 'd3;
        M_3[1] = 'd11;
        M_3[2] = 'd1;
        M_3[3] = 'd7;
        M_4[0] = 'd15;
        M_4[1] = 'd13;
        M_4[2] = 'd7;
        M_4[3] = 'd5;
    end

    always_comb begin
        case (county)                       //rescata el valor del mapa de threshold
            'd0:
                m_value = M_1[countx];
            'd1:
                m_value = M_2[countx];
            'd2:
                m_value = M_3[countx];
            'd3:
                m_value = M_4[countx];
         endcase     
            
        if (SW) begin
            out_red = (red[n-5:n-8] > m_value)?(red[n-1:n-4]+'d1):red[n-1:n-4];    //canales redondeados a 4 bits por color
            out_blue = (blue[n-5:n-8] > m_value)?(blue[n-1:n-4]+'d1):blue[n-1:n-4];
            out_green = (green[n-5:n-8] > m_value)?(green[n-1:n-4]+'d1):green[n-1:n-4];
            case (red[n-1:n-4])
                'd15:
                    out_red = red[n-1:n-4];
            endcase
            case (blue[n-1:n-4])
                'd15:
                out_blue = blue[n-1:n-4];
            endcase
            case (green[n-1:n-4])
                'd15:
                out_green = green[n-1:n-4];
            endcase
        end else begin
            out_red = red[n-1:n-4];
            out_blue = blue[n-1:n-4];
            out_green = green[n-1:n-4];
        end
    end
endmodule

module Grayscale(
    input logic [23:0] color_in,
    input logic SW,
    output logic [23:0] color_out);
    
    logic [7:0]  red;  
    logic [7:0]  green;
    logic [7:0]  blue;    
    logic [23:0] colorgris;
  
    always_comb
        begin
            red=color_in[23:16];    
            green=color_in[15:8];  
            blue=color_in[7:0];  
           
            if (SW==1'b1)
                if((red >= green) && (red >= blue))  
                    color_out= {red,red,red};
                else  
                    if((green >= red) && (green >= blue))  
                        color_out= {green,green,green};
                    else
                        color_out= {blue,blue,blue};
            else
                color_out = color_in; 
        end
endmodule

module Color_Scramble(
    input logic [23:0] color_in,
    input logic [5:0] SW,
    input logic SW_ON,
    output logic [23:0] color_out);
   
    logic [7:0]  red;  
    logic [7:0]  green;
    logic [7:0]  blue;    
   
    always_comb begin
        if (SW_ON==1'b1) begin
            case(SW[1:0])
                2'b00: red = color_in[7:0];    // canal rojo
                2'b01: red = color_in[15:8];   // canal verde
                2'b10: red = color_in[23:16];  // canal azul
                2'b11: red = 8'b0;
            endcase
            case(SW[3:2])
                2'b00: green = color_in[7:0];   // canal rojo
                2'b01: green = color_in[15:8];  // canal original
                2'b10: green = color_in[23:16]; // canal azul
                2'b11: green = 8'b0;
            endcase
            case(SW[5:4])
               2'b00: blue = color_in[7:0];    // canal rojo
               2'b01: blue = color_in[15:8];   // canal original
               2'b10: blue = color_in[23:16];  // canal azul
               2'b11: blue = 8'b0;
            endcase
            color_out = {blue,green,red};
        end
        else
            color_out = color_in; 
    end      
endmodule

module Sobel(
    input logic [23:0] color_in,
    input logic SW,
    output logic [23:0] color_out);
   
    logic [7:0]  red;  
    logic [7:0]  green;
    logic [7:0]  blue;
    
    logic [23:0] imagen_gray;
   					
   	Grayscale f4_Sobel_GrayScale(
        .color_in(color_in),
        .SW(SW),
        .color_out(imagen_gray));
   	
    always_comb begin
        if (SW==1'b1) begin
            color_out = imagen_gray; 
        end
        else
            color_out = color_in; 
    end      
endmodule


