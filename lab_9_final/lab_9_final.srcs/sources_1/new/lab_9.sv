`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 27.08.2019 16:56:24
// Design Name: 
// Module Name: lab_9
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lab_9(
    input logic CLK100MHZ,
    input logic [15:0]SW,
    input logic CPU_RESETN,
    input logic uart_rx_usb,
    output logic VGA_HS,
    output logic VGA_VS,
    output logic [3:0] VGA_R,
    output logic [3:0] VGA_G,
    output logic [3:0] VGA_B,
    output logic [15:0] LED);
    
	logic clkwiz_100mhz;
	logic clkwiz_82mhz;
    logic reset_db;
    logic [11:0] VGA_COLOR;  
    logic [10:0] vc_visible,hc_visible;
    
    logic rx_ready, rx_ready_12, rx_line;

    logic [7:0] rx_data;
    logic [23:0] video_data, pixel_inicial;
    logic [11:0] pixel_final;
    logic [17:0] addra, addrb;
    logic reset_addra, addra_count_en;
    
    assign rx_line = uart_rx_usb;
    assign addra_count_en = 1'b1;
    assign LED = addra[15:0];
    
////////////////////////////////////////////////
//Reloj de la salida VGA
//
    clk_wiz_0 ins(
        .clk_out2(clkwiz_82mhz),
        .clk_out1(clkwiz_100mhz),
        .locked(),
        .clk_in1(CLK100MHZ),
        .reset(1'b0));
        
/////////////////////////////////////////////////
//Debouncers
//
    pb_debouncer resetn_db(
        .clk(clkwiz_100mhz),
        .rst(0),
        .PB(~CPU_RESETN),
        .PB_state(),
        .PB_negedge(),
        .PB_posedge(reset_db));

////////////////////////////////////////////////
//UART
//
    uart_basic #(.BAUD_RATE(230400)) uart_(
        .clk(clkwiz_100mhz),
        .reset(1'b0),
        .rx(rx_line),
        .rx_data(rx_data),
        .rx_ready(rx_ready),
        .tx(),
        .tx_busy(),
        .tx_start(1'b0),
        .tx_data(8'b0));

    UART_RX_CTRL video_input_port(
        .clock(clkwiz_100mhz),
        .reset(1'b0),
        .rx_ready(rx_ready),
        .rx_data(rx_data),
        .video_data(video_data),
        .stateID(),
        .rx_ready_12(rx_ready_12));
        
/////////////////////////////////////////////////////
//Logica para guardar los datos con
//reloj A sincronizado con la UART y reloj B sincronizado
//con la salida VGA.
//    
    blk_mem_gen_0 blkmem(
        .clka(clkwiz_100mhz),
        .addra(addra),
        .dina(video_data),
        .wea(rx_ready_12),
        .clkb(clkwiz_82mhz),
        .addrb(addrb),
        .doutb(pixel_inicial));

    counter_nbit #(18) addra_counter(
        .clk(rx_ready_12),
        .reset(reset_addra),
        .enable(addra_count_en),
        .P(addra));

/////////////////////////////////////////////////////
// Driver de la pantalla
//
    driver_vga_1024x768(
        .clk_vga(clkwiz_82mhz),                    
        .hs(VGA_HS),
        .vs(VGA_VS),
        .hc_visible(hc_visible),
        .vc_visible(vc_visible));

//////////////////////////////////////////
//Filtros
//
    logic [23:0] pixel_filtro_1, pixel_filtro_2, pixel_filtro_3, pixel_filtro_4;
    logic [10:0] countx, county;
    logic ready_save_sobel = 1'd0;
    
    Dithering f1_Dithering(
        .color_in(pixel_inicial),
        .county(countx[1:0]),
        .countx(county[1:0]),
        .SW(SW[0]),
        .color_out(pixel_filtro_1));

    Grayscale f2_GrayScale(
        .color_in(pixel_filtro_1),
        .SW(SW[1]),
        .color_out(pixel_filtro_2));

    Color_Scramble f3_Scramble(
        .color_in(pixel_filtro_2),
        .SW(SW[15:10]),
        .SW_ON(SW[2]),
        .color_out(pixel_filtro_3));
    
    always_comb  
        if (rx_ready_12)
            ready_save_sobel = 1'd1;
        else 
            ready_save_sobel = 1'd0;
            
    Sobel f4_Sobel(
        .clk(clkwiz_82mhz),
        .color_in(pixel_filtro_3),
        .ready(ready_save_sobel),
        .SW(SW[3]),
        .reset_db(reset_db),
        .color_out(pixel_filtro_4));

    assign countx = ((hc_visible-1) % 4);
    assign county = ((vc_visible-1) % 4);
    
/////////////////////////////////////////////////////
//Configuracion salida VGA
//
    localparam CUADRILLA_XI     = 11'd256;
    localparam CUADRILLA_XF     = CUADRILLA_XI + 11'd512;
     
    localparam CUADRILLA_YI     = 11'd188;
    localparam CUADRILLA_YF     = CUADRILLA_YI + 11'd384;
    localparam COLOR_BLUE       = 12'h00F;
    localparam COLOR_YELLOW     = 12'hFF0;
    localparam COLOR_RED        = 12'hF00;
    localparam COLOR_BLACK      = 12'h000;
    localparam COLOR_WHITE      = 12'hFFF;
    localparam COLOR_CYAN       = 12'h0FF;
    localparam COLOR_GREY       = 12'h444;
        
    logic DENTRO_DE_LA_PANTALLA;
    logic DENTRO_DE_LA_CUADRILLA;
    assign DENTRO_DE_LA_PANTALLA = (( hc_visible != 0) && ( vc_visible != 0));
    assign DENTRO_DE_LA_CUADRILLA = ((hc_visible > CUADRILLA_XI) && (hc_visible <= CUADRILLA_XF) 
    && (vc_visible > CUADRILLA_YI) && (vc_visible <= CUADRILLA_YF));
    
    assign pixel_final = {pixel_filtro_4[23:20], pixel_filtro_4[15:12], pixel_filtro_4[7:4]};

    always @(posedge clkwiz_82mhz ) begin
        if(DENTRO_DE_LA_PANTALLA)
            if(DENTRO_DE_LA_CUADRILLA) begin
                VGA_COLOR <= {pixel_final};
            end 
            else
                VGA_COLOR  <= COLOR_GREY;
            else
                VGA_COLOR  <= COLOR_BLACK;
    end
    
    assign {VGA_R, VGA_G, VGA_B} = VGA_COLOR;  
    
    assign addrb = ({7'b0, vc_visible} - 18'd1 - {7'b0, CUADRILLA_YI})*512 + ({7'b0,hc_visible} + 18'd2 - {7'b0, CUADRILLA_XI});

    always @(posedge clkwiz_100mhz)
        reset_addra = ((addra == 18'd196608) | reset_db) ? 1'b1 : 1'b0;
    
endmodule
